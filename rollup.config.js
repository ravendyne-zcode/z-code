import babel from 'rollup-plugin-babel'
import execute from 'rollup-plugin-execute'

export default {
    entry: 'zbuild/main.js',
    indent: '    ',
    // sourceMap: true,

    plugins: [
        babel({
            exclude: 'node_modules/**',
        }),
    ],

    targets: [
        {
            format: 'umd',
            moduleName: 'ZBUILD',
            dest: 'bin/zbuild.rollup.js'
        },
    ],

    plugins: [
        execute( 'node bin/add_version.js' ),
    ],

/*
    external: [
        'lodash',
        ...
    ],

    globals: {
        lodash: 'lodash',
        ...
    },
*/
}
