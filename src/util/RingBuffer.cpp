#include <util/RingBuffer.hpp>
#include <cstring>


namespace util
{

    //---------------------------------------------------------------------------------------
    //
    // PUBLIC methods
    //

    RingBuffer::RingBuffer( ByteBuffer &buffer, uint32_t itemSize )
    {
        this->init( buffer.buffer, buffer.length, itemSize );
    }

    RingBuffer::RingBuffer( void *buffer, uint32_t length, uint32_t itemSize )
    {
        this->init( buffer, length, itemSize );
    }

    RingBuffer::RingBuffer()
    {
        this->_buffer = NULL;
        this->_length = 0;
        this->_itemCount = 0;
        this->_itemSize = 1;
        this->_head = 0;
    }

    void RingBuffer::init( void *buffer, uint32_t length, uint32_t itemSize )
    {
        this->_buffer = ( uint8_t* ) buffer;
        this->_length = length;
        this->_itemCount = 0;
        this->_itemSize = itemSize;
        this->_head = 0;
    }

    bool RingBuffer::isEmpty()
    {
        return this->availableData() == 0;
    }

    bool RingBuffer::isFull()
    {
        return this->availableData() == this->capacity();
    }

    uint32_t RingBuffer::availableData()
    {
        return this->_itemCount;
    }

    uint32_t RingBuffer::availableSpace()
    {
        return this->capacity() - this->availableData();
    }

    uint32_t RingBuffer::push( const void *data, uint32_t itemCount )
    {
        uint32_t spaceBetweenTailAndEnd;
        uint32_t chunkACount;
        uint32_t chunkBCount;

        if( this->isFull() )
        {
            return 0;
        }

        // This makes sure we don't overrun the head (and thus the buffer)
        if( itemCount > this->availableSpace() )
        {
            itemCount = this->availableSpace();
        }

        spaceBetweenTailAndEnd = this->capacity() - this->tail();

        if( itemCount < spaceBetweenTailAndEnd )
        {
            this->pushUnchecked( data, itemCount );
        }
        else
        {
            chunkACount = spaceBetweenTailAndEnd;
            chunkBCount = itemCount - chunkACount;

            data = this->pushUnchecked( data, chunkACount );
            this->pushUnchecked( data, chunkBCount );
        }

        return itemCount;
    }

    uint32_t RingBuffer::pushOne( const void *data )
    {
        void *ptr;

        if( this->isFull() )
        {
            return 0;
        }

        // get tail position
        ptr = this->tailPtr();

        // save data where tail is
        std::memcpy( ptr, data, this->_itemSize );

        // move tail forward
        this->tailAdvanceOne();

        return 1;
    }

    uint32_t RingBuffer::pop( void *dataBuffer, uint32_t itemCount )
    {
        uint32_t spaceBetweenHeadAndEnd;
        uint32_t chunkACount;
        uint32_t chunkBCount;

        if( this->isEmpty() )
        {
            return 0;
        }

        // this makes sure we don't overrun the tail (and thus the buffer)
        if( itemCount > this->availableData() )
        {
            itemCount = this->availableData();
        }

        if( this->head() < this->tail() )
        {
            this->popUnchecked( dataBuffer, itemCount );
        }
        else
        {
            spaceBetweenHeadAndEnd = this->capacity() - this->head();
            chunkACount = spaceBetweenHeadAndEnd;
            if( itemCount < chunkACount )
            {
                dataBuffer = this->popUnchecked( dataBuffer, itemCount );
            }
            else
            {
                chunkBCount = itemCount - chunkACount;

                dataBuffer = this->popUnchecked( dataBuffer, chunkACount );
                this->popUnchecked( dataBuffer, chunkBCount );
            }
        }

        return itemCount;
    }

    uint32_t RingBuffer::popOne( void *dataBuffer )
    {
        void *ptr;

        if( this->isEmpty() )
        {
            return 0;
        }

        // get head position
        ptr = this->headPtr();

        // get data from where head is
        std::memcpy( dataBuffer, ptr, this->_itemSize );

        // move head forward
        this->headAdvanceOne();

        return 1;
    }

    uint32_t RingBuffer::peek( void *dataBuffer )
    {
        void *ptr;

        if( this->isEmpty() )
        {
            return 0;
        }

        // get head position
        ptr = this->headPtr();

        // get data from where head is
        std::memcpy( dataBuffer, ptr, this->_itemSize );

        return 1;

    }

    void RingBuffer::flush()
    {
        this->_itemCount = 0;
        this->_head = 0;
    }

    //---------------------------------------------------------------------------------------
    //
    // PRIVATE methods
    //

    uint32_t RingBuffer::capacity()
    {
        return this->_length;
    }

    // We need this one since we don't keep track of tail
    uint32_t RingBuffer::tail()
    {
        uint32_t tail;

        tail = this->_head + this->_itemCount;

        // wrap around
        if( tail >= this->_length )
        {
            tail = tail - this->_length;
        }

        return tail;
    }
    // This one is just for consistency sake
    uint32_t RingBuffer::head()
    {
        return this->_head;
    }

    void* RingBuffer::headPtr()
    {
        uint8_t *ptr = this->_buffer;

        ptr += this->_head * this->_itemSize;

        return ( void* ) ptr;
    }

    void* RingBuffer::tailPtr()
    {
        uint8_t *ptr = this->_buffer;
        uint32_t tail;

        tail = this->tail();
        ptr += tail * this->_itemSize;

        return ( void* ) ptr;
    }

    void RingBuffer::headAdvance( uint32_t count )
    {
        this->_head += count;

        // wrap around
        if( this->_head >= this->_length )
        {
            this->_head -= this->_length;
        }

        this->_itemCount -= count;
    }

    void RingBuffer::headAdvanceOne()
    {
        this->_head++;

        // wrap around
        if( this->_head == this->_length )
        {
            this->_head = 0;
        }

        this->_itemCount--;
    }

    void RingBuffer::tailAdvance( uint32_t count )
    {
        this->_itemCount += count;
    }

    void RingBuffer::tailAdvanceOne()
    {
        this->_itemCount++;
    }

    void* RingBuffer::pushUnchecked( const void *data, uint32_t itemCount )
    {
        void *ptr;

        // get tail position
        ptr = this->tailPtr();

        // save data where tail is
        std::memcpy( ptr, data, this->_itemSize * itemCount );

        // move tail forward
        this->tailAdvance( itemCount );

        ptr = ( void* )( ( uint8_t* ) data + this->_itemSize * itemCount );

        return ptr;
    }

    void* RingBuffer::popUnchecked( void *data, uint32_t itemCount )
    {
        void *ptr;

        // get head position
        ptr = this->headPtr();

        // save data
        std::memcpy( data, ptr, this->_itemSize * itemCount );

        // move head forward
        this->headAdvance( itemCount );

        ptr = ( void* )( ( uint8_t* ) data + this->_itemSize * itemCount );

        return ptr;
    }

    uint32_t RingBuffer::spaceBetweenTailAndEnd()
    {
        return this->capacity() - this->tail();
    }

}
