#include <util/ByteBuffer.hpp>

namespace util
{

    ByteBuffer::ByteBuffer( uint8_t* buffer, uint32_t length )
    {
        this->buffer = buffer;
        this->length = length;
    }

}
