#include <util/Utils.hpp>

namespace util
{

    unsigned char *strrev( unsigned char *str, int len )
    {
        unsigned char *p1, *p2;

        if( ! str || ! *str )
        {
            return str;
        }

        for( p1 = str, p2 = str + len - 1; p2 > p1; ++p1, --p2 )
        {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
        }

        return str;
    }

    // returns the number of characters placed into str buffer
    // during conversion, not counting terminating '\0'.
    // If return value is < 0, that means we have ran out of space
    // in str buffer during conversion.
    // Essentially, if result > 0 conversion was successful and the
    // return value is number of bytes occupied in the passed in buffer,
    // not counting terminating '\0'.
    int itoa( int num, int base, char* str, int len )
    {
        int sum = num;
        int i = 0;
        int digit;

        if( len == 0 )
        {
            return 0;
        }

        do
        {

            // Get the rightmost digit of the number, in the base passed in.
            digit = sum % base;
            
            // Convert the digit into ASCII char
            if( digit < 0xA )
            {
                str[ i ] = '0' + digit;
                i++;
            }
            else
            {
                str[ i ] = 'A' + digit - 0xA;
                i++;
            }
            
            // Shift the number to the right, in the base passed in.
            sum /= base;

            // Keep working until either we finished with the number ( sum == 0 )
            // or we ran out of space in the buffer ( i < len - 1 )
        } while( sum && ( i < (len - 1) ) );

        // Check if we have come to the end of the string buffer
        // and haven't finished converting the entire number.
        if( i == (len - 1) && sum )
        {
            return -1;
        }

        str[ i ] = '\0';

        // since we are shifting the number to the right,
        // and filling the passed in buffer from the left,
        // the resulting string will contain digits of the number in reverse order,
        // so we have to reverse the result to get the correct representation.
        strrev( (unsigned char *) str, i );

        return i;
    }

    int pow( int num, int exp )
    {
        if( exp > 0 )
        {
            return num * pow( num, exp - 1 );
        }

        return 1;
    }

    // Converts float number to decimal-point string representation.
    // Will return size of space used in the str buffer to represent the number,
    // or value < 0 if we ran out of space.
    int ftoa( float num, int precision, char* str, int len )
    {
        // Extract integer part
        int ipart = (int) num;

        // Extract floating part
        float fpart = num - (float) ipart;

        int i = itoa( ipart, 10, str, len );

        if( i > 0 && precision != 0 )
        {
            str[ i ] = '.';
            i++;

            fpart = fpart * pow( 10, precision );

            int j = itoa( (int) fpart, 10, str + i, len - i );

            if( j < 0 )
            {
                return j;
            }

            i += j;
        }

        return i;
    }

}
