// zcode
#include <zcore>
#include <soc/chip_lpc1347>

// lpc_open
#include "chip.h"





namespace gpio
{
    hal::Pin::Pin( int halPinNumber ) : hal::Pin::Pin( hal::halPinToSocPort( halPinNumber ), hal::halPinToSocPin( halPinNumber ) )
    {
        this->halPinNumber = halPinNumber;
    }

    hal::Pin::Pin( int socPortNumber, int socPinNumber )
    {
        this->halPinNumber = NOT_A_HAL_PIN;
        this->port = socPortNumber;
        this->pin = socPinNumber;

        this->mode( PinMode::GPIO );
        this->direction( PinDir::Input );
    }

    uint32_t hal::Pin::getHalPinNumber()
    {
        if( NOT_A_HAL_PIN != this->halPinNumber )
        {
            return this->halPinNumber;
        }

        uint8_t port = this->port;
        uint8_t pin = this->pin;

        for( uint32_t idx = 0; idx < gpio::hal::halPinCount; idx++ )
        {
            if( gpio::hal::HalPinMap[ idx ].port == port && gpio::hal::HalPinMap[ idx ].pin == pin )
            {
                return idx;
            }
        }

        return NOT_A_HAL_PIN;
    }

    void hal::Pin::mode( PinMode mode )
    {
        uint32_t pinIdx = getHalPinNumber();
        // TODO: what if NOT_A_HAL_PIN == pinIdx ?? Do nothing, since ALL possible pins for the board should be covered by HAL pin arrays ??

        uint32_t modefunc = PIN_FUNCTION_NONE;

        switch( mode )
        {
            case PinMode::GPIO:
                modefunc = PinModeGPIO[ pinIdx ];
            break;

            case PinMode::ADC:
                modefunc = PinModeADC[ pinIdx ];
            break;

            case PinMode::PWM:
                modefunc = PinModePWM[ pinIdx ];
            break;

            default:
                // PinMode::UserDefined
            break;
        }

        if( PIN_FUNCTION_NONE != modefunc && PIN_FUNCTION_RESERVED != modefunc )
        {
            Chip_IOCON_PinMuxSet( LPC_IOCON, this->port, this->pin, modefunc );
        }
    }

    void hal::Pin::high()
    {
        Chip_GPIO_WritePortBit( LPC_GPIO_PORT, this->port, this->pin, true );
    }

    void hal::Pin::low()
    {
        Chip_GPIO_WritePortBit( LPC_GPIO_PORT, this->port, this->pin, false );
    }

    void hal::Pin::toggle()
    {
        Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, this->port, this->pin);
    }

    void hal::Pin::direction( PinDir direction )
    {
        bool dir = direction == PinDir::Output ? true : false;

        Chip_GPIO_WriteDirBit( LPC_GPIO_PORT, this->port, this->pin, dir );
    }

    bool hal::Pin::read()
    {
        return (bool) Chip_GPIO_GetPinState( LPC_GPIO_PORT, this->port, this->pin );
    }
}
