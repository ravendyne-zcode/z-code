// zcode
#include <zcore>
#include <soc/chip_lpc1347>
#include <util/RingBuffer.hpp>

#include <compiler/section_macros.hpp>

// lpc_open
#include "chip.h"

/*****************************************************************************
 * I2C debug/trace stuff
 ****************************************************************************/
#define DEBUG_I2C
#ifdef DEBUG_I2C
typedef struct _i2cdebuginfo
{
uint8_t state;
uint8_t dat;
uint8_t conset;
} i2cdebuginfo;
i2cdebuginfo i2cdebugbuff[512];
uint32_t i2cbuffidx2 = 0;
void I2C_RECORD_STATE(void)
{
    i2cdebugbuff[i2cbuffidx2].state = LPC_I2C->STAT;
    i2cdebugbuff[i2cbuffidx2].dat = LPC_I2C->DAT;
    i2cdebugbuff[i2cbuffidx2].conset = LPC_I2C->CONSET;
    i2cbuffidx2++;
    i2cbuffidx2 &= 511;
}
#else
#define I2C_RECORD_STATE()
#endif



static uint32_t I2C_CON_FLAGS = ( I2C_CON_AA | I2C_CON_SI | I2C_CON_STO | I2C_CON_STA );

namespace i2c
{
    void hal::initialize()
    {
    }

    #define I2C_RBR_SIZE 64

    // Place I2C RX/TX buffer to LPC1347 peripheral RAM
    __SECTION_BSS_(PeripheralRam) uint8_t I2C_rbr_buffer[ I2C_RBR_SIZE ];

    util::RingBuffer I2C_rbr( I2C_rbr_buffer, I2C_RBR_SIZE, sizeof( I2C_rbr_buffer[ 0 ] ) );

    class I2CContext : public hal::I2C
    {
    public:
        I2CContext( int i2cDeviceNumber, util::RingBuffer* rbr );

        void init( int i2cDeviceNumber );
        virtual void start();
        virtual void restart();
        virtual void stop();
        virtual void ack();
        virtual void nack();

        virtual void address( uint16_t address );
        virtual void addressModeRead();
        virtual void addressModeWrite();

        virtual i2c::hal::Event lastEvent();

        virtual bool writeAddress();
        virtual bool write( uint8_t byte );
        virtual bool read( uint8_t *byte );

        virtual void onEvent( I2cEventCallback callback );

        virtual void initialize();
        virtual void enableInterrupt();
        virtual void disableInterrupt();
        virtual void master();
        virtual void slave();
        virtual void transferBlocking(uint8_t address, uint8_t *txBuffer, uint16_t txSize, uint8_t *rxBuffer, uint16_t rxSize);

        void interruptHandler();

    private:
        LPC_I2C_T* i2c;
        IRQn_Type irq;
        util::RingBuffer *rbr;
        uint8_t slaveAddress;
        I2cEventCallback i2cEventCallback;

        // we start with all flags cleared
        // and then set each depending on current i2c state.
        // since we sould NOT write ONE's to reserved bits
        // of either CONCLR or CONSET, it is easier to work
        // with flags using CONCLR and then simply fill in
        // CONSET with reversed value of flagsCONCLR
        uint32_t flagsCONCLR;

        void clearFlags() { this->flagsCONCLR = I2C_CON_FLAGS; }
        void applyFlags()
        {
            // clear the flags marked for clearing
            this->i2c->CONCLR = this->flagsCONCLR;
            // set the flags that are not marked for clearing
            this->i2c->CONSET = this->flagsCONCLR ^ I2C_CON_FLAGS;
        }
        void setSTA() { this->flagsCONCLR &= ~I2C_CON_STA; }
        void clearSTA() { this->flagsCONCLR |= I2C_CON_STA; }
        void setSTO() { this->flagsCONCLR &= ~I2C_CON_STO; }
        void clearSTO() { this->flagsCONCLR |= I2C_CON_STO; }
        void setAA() { this->flagsCONCLR &= ~I2C_CON_AA; }
        void clearAA() { this->flagsCONCLR |= I2C_CON_AA; }

        void i2cEnable() { this->i2c->CONSET |= I2C_CON_I2EN; }
        void i2cDisable() { this->i2c->CONCLR |= I2C_CON_I2EN; }
    };

    I2CContext::I2CContext( int i2cDeviceNumber, util::RingBuffer* rbr )
    {
        this->rbr = rbr;
        if( 0 == i2cDeviceNumber )
        {
            this->i2c = LPC_I2C;
            this->irq = I2C0_IRQn;
        }
        else
        {
        }

        this->clearFlags();
    }

// ----------------------------------------------------------------------------
#define SPEED_10KHZ          10000
#define SPEED_100KHZ         100000
#define SPEED_400KHZ         400000


    void I2CContext::initialize()
    {
        this->rbr->flush();

        // I2C pins are initialized in board specifc pins related source
        // Init_I2C_PinMux();

        // Chip_I2C_Init(I2C0);
        // TODO: unfix clock and reset
        Chip_Clock_EnablePeriphClock( SYSCTL_CLOCK_I2C );
        Chip_SYSCTL_PeriphReset( RESET_I2C0 );

        // TODO: unfix speed
        // Chip_I2C_SetClockRate(I2C0, SPEED_100KHZ);
        const uint32_t i2cSpeed = SPEED_10KHZ;
        uint32_t SCLValue = ( Chip_Clock_GetSystemClockRate() / i2cSpeed );
        this->i2c->SCLH = (uint32_t) ( SCLValue >> 1 );
        this->i2c->SCLL = (uint32_t) ( SCLValue - this->i2c->SCLH );

        this->clearFlags();
        /* Set I2C operation to default */
        this->i2c->CONCLR = this->flagsCONCLR;

        this->i2cDisable();
    }

    void I2CContext::enableInterrupt()
    {
        NVIC_EnableIRQ( this->irq );
    }

    void I2CContext::disableInterrupt()
    {
        NVIC_DisableIRQ( this->irq );
    }

    void I2CContext::master()
    {
//        this->clearFlags();
        /* Reset STA, STO, SI, AA */
//        this->i2c->CONCLR = this->flagsCONCLR;
        this->i2c->CONCLR = I2C_CON_SI | I2C_CON_STO | I2C_CON_STA | I2C_CON_AA;


        /* Enter to Master Transmitter mode */
        this->i2c->CONSET = I2C_CON_I2EN | I2C_CON_STA;
//        this->i2cEnable();
//        this->setSTA();
        // we must apply flags in order to trigger
        // I2C hardware to do anything
//        this->applyFlags();
    }

    void I2CContext::slave()
    {
    }

    bool I2CContext::write( uint8_t byte )
    {
        // This will have effect only when SI = 0
        this->i2c->DAT = byte;

        return true;
    }

    bool I2CContext::read( uint8_t *byte )
    {
        // This will get the real data only when SI = 0
        *byte = this->i2c->DAT;

        return true;
    }

    void I2CContext::start()
    {
        this->clearSTO();
        this->setSTA();
    }

    void I2CContext::restart()
    {
        this->start();
    }

    void I2CContext::stop()
    {
        this->clearSTA();
        this->setSTO();
    }

    void I2CContext::ack()
    {
        this->setAA();
    }

    void I2CContext::nack()
    {
        this->clearAA();
    }

    void I2CContext::address( uint16_t address )
    {
        this->slaveAddress = ( address & 0x8F ) << 1;
    }

    void I2CContext::addressModeRead()
    {
        this->slaveAddress |= 0x01;
    }

    void I2CContext::addressModeWrite()
    {
        this->slaveAddress &= 0xFE;
    }

    void I2CContext::onEvent( I2cEventCallback callback )
    {
        this->i2cEventCallback = callback;
    }

    i2c::hal::Event I2CContext::lastEvent()
    {
        I2C_RECORD_STATE();
        int i2cStateRegister = ( this->i2c->STAT & I2C_STAT_CODE_BITMASK );

        // Master mode states only
        switch( i2cStateRegister )
        {
            case 0x08:      /* Start condition on bus */
            case 0x10:      /* Repeated start condition */
                return i2c::hal::Event::START_SENT;

            /* Tx handling */
            case 0x18:      /* SLA+W sent and ACK received */
            case 0x28:      /* DATA sent and ACK received */
            case 0x40:      /* SLA+R sent and ACK received */
                return i2c::hal::Event::DATA_SENT_ACK_RECEIVED;

            /* Rx handling */
            case 0x58:      /* Data Received and NACK sent */
            case 0x50:      /* Data Received and ACK sent */
                return i2c::hal::Event::DATA_RECEIVED;

            /* NAK Handling */
            case 0x20:      /* SLA+W sent NAK received */
            case 0x30:      /* DATA sent NAK received */
            case 0x48:      /* SLA+R sent NAK received */
                return i2c::hal::Event::DATA_SENT_NACK_RECEIVED;

            case 0x38:      /* Arbitration lost */
                return i2c::hal::Event::ARBITRATION_LOST;

            /* Bus Error */
            case 0x00:
                return i2c::hal::Event::BUS_ERROR;

            /* No events, SI = 0 */
            case 0xF8:
                return i2c::hal::Event::IDLE;
        }

        return i2c::hal::Event::IDLE;
    }

    bool I2CContext::writeAddress()
    {
        this->write( this->slaveAddress );

        return true;
    }

    extern i2c::I2CContext I2C_Context;

    uint32_t E__Chip_I2CM_XferHandler(LPC_I2C_T *pI2C, I2CM_XFER_T *xfer)
    {
        i2c::hal::Event event = I2C_Context.lastEvent();

        switch( event )
        {
//            case 0x08:      /* Start condition on bus */
//            case 0x10:      /* Repeated start condition */
//                return i2c::hal::Event::START_SENT;
            case i2c::hal::Event::START_SENT:
            break;

            /* Tx handling */
//            case 0x18:      /* SLA+W sent and ACK received */
//            case 0x28:      /* DATA sent and ACK received */
//            case 0x40:      /* SLA+R sent and ACK received */
//                return i2c::hal::Event::DATA_SENT_ACK_RECEIVED;
            case i2c::hal::Event::DATA_SENT_ACK_RECEIVED:
            break;

            /* Rx handling */
//            case 0x58:      /* Data Received and NACK sent */
//            case 0x50:      /* Data Received and ACK sent */
//                return i2c::hal::Event::DATA_RECEIVED;
            case i2c::hal::Event::DATA_RECEIVED:
            break;

            /* NAK Handling */
//            case 0x20:      /* SLA+W sent NAK received */
//            case 0x30:      /* DATA sent NAK received */
//            case 0x48:      /* SLA+R sent NAK received */
//                return i2c::hal::Event::DATA_SENT_NACK_RECEIVED;
            case i2c::hal::Event::DATA_SENT_NACK_RECEIVED:
            break;

//            case 0x38:      /* Arbitration lost */
//                return i2c::hal::Event::ARBITRATION_LOST;
            case i2c::hal::Event::ARBITRATION_LOST:
            break;

            /* Bus Error */
//            case 0x00:
//                return i2c::hal::Event::BUS_ERROR;
            case i2c::hal::Event::BUS_ERROR:
            break;

            /* No events, SI = 0 */
//            case 0xF8:
//                return i2c::hal::Event::IDLE;
            case i2c::hal::Event::IDLE:
            break;
        }

        return 0;
    }

    /* Master transfer state change handler handler */
    uint32_t __Chip_I2CM_XferHandler(LPC_I2C_T *pI2C, I2CM_XFER_T *xfer)
    {
        uint32_t cclr = I2C_CON_FLAGS;
        I2C_RECORD_STATE();
        uint32_t currentState = pI2C->STAT & I2C_STAT_CODE_BITMASK;

        switch ( currentState ) {
        case 0x08:      /* Start condition on bus */
        case 0x10:      /* Repeated start condition */
            pI2C->DAT = (xfer->slaveAddr << 1) | (xfer->txSz == 0);
            break;

        /* Tx handling */
        case 0x20:      /* SLA+W sent NAK received */
        case 0x30:      /* DATA sent NAK received */
//            if ((xfer->options & I2CM_XFER_OPTION_IGNORE_NACK) == 0) {
                xfer->status = I2CM_STATUS_NAK;
                cclr &= ~I2C_CON_STO;
                break;
//            }

        case 0x18:      /* SLA+W sent and ACK received */
        case 0x28:      /* DATA sent and ACK received */
            if (!xfer->txSz) {
                if (xfer->rxSz) {
                    cclr &= ~I2C_CON_STA;
                }
                else {
                    xfer->status = I2CM_STATUS_OK;
                    cclr &= ~I2C_CON_STO;
                }

            }
            else {
                pI2C->DAT = *xfer->txBuff++;
                xfer->txSz--;
            }
            break;

        /* Rx handling */
        case 0x58:      /* Data Received and NACK sent */
        case 0x50:      /* Data Received and ACK sent */
            *xfer->rxBuff++ = pI2C->DAT;
            xfer->rxSz--;

        case 0x40:      /* SLA+R sent and ACK received */
            if ((xfer->rxSz > 1) /*|| (xfer->options & I2CM_XFER_OPTION_LAST_RX_ACK)*/) {
                cclr &= ~I2C_CON_AA;
            }
            if (xfer->rxSz == 0) {
                xfer->status = I2CM_STATUS_OK;
                cclr &= ~I2C_CON_STO;
            }
            break;

        /* NAK Handling */
        case 0x48:      /* SLA+R sent NAK received */
            xfer->status = I2CM_STATUS_SLAVE_NAK;
            cclr &= ~I2C_CON_STO;
            break;

        case 0x38:      /* Arbitration lost */
            xfer->status = I2CM_STATUS_ARBLOST;
            break;

        case 0x00:      /* Bus Error */
            xfer->status = I2CM_STATUS_BUS_ERROR;
            cclr &= ~I2C_CON_STO;
            break;

        default:
            xfer->status = I2CM_STATUS_ERROR;
            cclr &= ~I2C_CON_STO;
            break;
        }

        /* Set clear control flags */
        pI2C->CONSET = cclr ^ I2C_CON_FLAGS;
        pI2C->CONCLR = cclr;

        return xfer->status;
    }

    /* Transmit and Receive data in master mode */
    uint32_t __Chip_I2CM_XferBlocking(LPC_I2C_T *pI2C, I2CM_XFER_T *xfer)
    {
//        uint32_t ret = 0;
        uint32_t ret = I2CM_STATUS_BUSY;
        /* start transfer */
        /* set the transfer status as busy */
        xfer->status = I2CM_STATUS_BUSY;
        /* Clear controller state. */
        /* Reset STA, STO, SI */
        pI2C->CONCLR = I2C_CON_SI | I2C_CON_STO | I2C_CON_STA | I2C_CON_AA;
        /* Enter to Master Transmitter mode */
        pI2C->CONSET = I2C_CON_I2EN | I2C_CON_STA;


        while (ret == I2CM_STATUS_BUSY) {
            /* wait for status change interrupt */
            while ( (pI2C->CONSET & I2C_CON_SI) == 0) {}

            /* call state change handler */
            ret = __Chip_I2CM_XferHandler(pI2C, xfer);
        }
        return ret;
    }

    void I2CContext::transferBlocking(uint8_t address, uint8_t *txBuffer, uint16_t txSize, uint8_t *rxBuffer, uint16_t rxSize)
    {
        I2CM_XFER_T xfer;

        xfer.slaveAddr = address;
        xfer.rxSz = rxSize;
        xfer.rxBuff = rxBuffer;
        xfer.txSz = txSize;
        xfer.txBuff = txBuffer;

        // returns: xfer->status != I2CM_STATUS_BUSY;
        //    Chip_I2CM_XferBlocking(LPC_I2C, &xfer);
//        Chip_I2CM_XferBlocking(this->i2c, &xfer);
        __Chip_I2CM_XferBlocking(this->i2c, &xfer);
    //    Serial.println(xfer.status);

        return;
//        while( this->lastEvent() != hal::Event::IDLE )
//        {
//            // wait for SI to become 1 (i2c state changed interrupt)
//            while ( (this->i2c->CONSET & I2C_CON_SI) == 0) {}
//
//            this->clearFlags();
//            this->i2cEventCallback( this->lastEvent() );
//            this->applyFlags();
//        }
    }

// ----------------------------------------------------------------------------

    void I2CContext::interruptHandler()
    {
        this->clearFlags();

        // call core interruptHandler()
        this->i2cEventCallback( this->lastEvent() );

        // apply CON flags.
        // this will also clear interrupt flag on every I2C IRQ service
        // since we never touch it after clearFlags() is called
        this->applyFlags();
    }

// ----------------------------------------------------------------------------

}


// ----------------------------------------------------------------------------
//   STANDARD SOC DEVICE: I2C
// ----------------------------------------------------------------------------

static i2c::I2CContext I2C_Context( 0, &i2c::I2C_rbr );

i2c::I2c I2c( ( i2c::hal::I2C* ) &I2C_Context );


// ----------------------------------------------------------------------------
//   I2C IRQ HANDLER
// ----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

/**
 * All LPC IRQ handlers and such have to be implemented here
 * within extern "C" declaration, otherwise their names will get mangled
 * by C++ compiler and they WON'T get linked where you would expect them
 * to be because they are declared with __attribute__((weak)).
 */

void I2C_IRQHandler(void)
{
    I2C_Context.interruptHandler();
}


#ifdef __cplusplus
}
#endif
