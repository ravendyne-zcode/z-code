// zcode
#include <zcore>
#include <soc/chip_lpc1347>
#include <util/RingBuffer.hpp>

#include <compiler/section_macros.hpp>


// lpc_open
#include "chip.h"


namespace serial
{

/* Transmit and receive ring buffer sizes */
#define UART_TXRB_SIZE 128   /* Send */
#define UART_RXRB_SIZE 32    /* Receive */

    // Place UART0 RX/TX buffer to LPC1347 peripheral RAM
    __SECTION_BSS_(PeripheralRam) uint8_t UART0_rx_buffer[ UART_RXRB_SIZE ];
    __SECTION_BSS_(PeripheralRam) uint8_t UART0_tx_buffer[ UART_TXRB_SIZE ];

    util::RingBuffer UART0_rx_rbr( UART0_rx_buffer, UART_RXRB_SIZE, sizeof( UART0_rx_buffer[ 0 ] ) );
    util::RingBuffer UART0_tx_rbr( UART0_tx_buffer, UART_TXRB_SIZE, sizeof( UART0_tx_buffer[ 0 ] ) );


    class UARTContext : public hal::UART
    {
    public:
        UARTContext( int uartNumber );

        void init( int uartNumber );

        // InputStream
        virtual bool read( uint8_t *byte );
        virtual int read( uint8_t *buffer, int count );

        // OutputStream
        virtual bool write( uint8_t byte );
        virtual int write( uint8_t *buffer, int count );

        void start();
        void stop();

        bool TxRegisterEmpty();
        bool TxRBREmpty();
        bool RxRegisterHasData();
        void Transmit();
        void Receive();
        void DisableTHRInterrupt();
        void EnableTHRInterrupt();
        uint32_t THRInterruptEnabled();

    private:
        LPC_USART_T* usart;
        IRQn_Type irq;
        util::RingBuffer *rx_rbr;
        util::RingBuffer *tx_rbr;
    };


    void hal::initialize()
    {
        // TX/RX pins are configured as TX/RX in board::setupPins()
        // and that's about the only thing we need to initialize
        // on system level for UARTs
    }

    UARTContext::UARTContext( int uartNumber )
    {
        // There's only one UART on LPC1347,
        // so we'll just ignore uartNumber.

        this->irq = UART0_IRQn;
        this->usart = LPC_USART;

        this->rx_rbr = &UART0_rx_rbr;
        this->tx_rbr = &UART0_tx_rbr;

        this->init( uartNumber );
    }


// ----------------------------------------------------------------------------

    void UARTContext::init( int uartNumber )
    {
        // There's only one UARt on LPC1347,
        // so we just ignore uartNumber and work with UART0.

        // Chip_UART_Init( this->usart );
        Chip_Clock_EnablePeriphClock( SYSCTL_CLOCK_UART0 );
        Chip_Clock_SetUARTClockDiv( 1 );
        /* Disable fractional divider */
        this->usart->FDR = 0x10;

        /* Setup UART for 115.2K8N1 */
        Chip_UART_SetBaud( this->usart, 115200 );
        Chip_UART_ConfigData( this->usart, ( UART_LCR_WLEN8 | UART_LCR_SBS_1BIT ) );
        Chip_UART_SetupFIFOS( this->usart, ( UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2 ) );
        Chip_UART_TXEnable( this->usart );

        /* Enable receive data and line status interrupt, the transmit interrupt
           is handled by the driver. */
        Chip_UART_IntEnable( this->usart, ( UART_IER_RBRINT | UART_IER_RLSINT ) );

        this->start();
    }

    bool UARTContext::write( uint8_t byte )
    {
        uint32_t byteSent;
        /*
         * We don't want UART IRQ handler to access ringbuffer  while
         * we fill it with data to be sent, so we'll disable UART
         * interrupt here
         */
        this->DisableTHRInterrupt();

        byteSent = this->tx_rbr->pushOne( &byte );

        // We have to do this call here in order to trigger
        // the first TX interrupt (there's one for each byte that needs to be sent)
        // and start off transmitting process.
        this->Transmit();

        this->EnableTHRInterrupt();

        return byteSent > 0;
    }

    bool UARTContext::read( uint8_t *byte )
    {
        if( this->rx_rbr->popOne( byte ) == 0 )
        {
            return false;
        }

        return true;
    }

    int UARTContext::write( uint8_t *buffer, int count )
    {
        /*
         * We don't want UART IRQ handler to access ringbuffer while
         * we fill it with data to be sent, so we'll disable UART
         * interrupt here
         */
        this->DisableTHRInterrupt();

        // Move as much data as possible into transmit ring buffer
        int bytesSent = this->tx_rbr->push( buffer, count );

        // If there's space in UART TX FIFO buffer, this will transfer
        // as much data as possible from our RBR to UART's FIFO
        // We have to do this call here in order to trigger
        // the first TX interrupt (there's one for each byte that needs to be sent)
        // and start off transmitting process.
        this->Transmit();

        // Enable UART transmit interrupt
        this->EnableTHRInterrupt();

        return bytesSent;
    }

    int UARTContext::read( uint8_t *buffer, int count )
    {
        return this->rx_rbr->pop( buffer, count );
    }

    void UARTContext::start()
    {
        /* preemption = 1, sub-priority = 1 */
        NVIC_SetPriority( this->irq, 1 );
        NVIC_EnableIRQ( this->irq );
    }

    void UARTContext::stop()
    {
        // Chip_UART_IntDisable( this->usart, ( UART_IER_RBRINT | UART_IER_RLSINT ) );

        /* DeInitialize UART0 peripheral */
        NVIC_DisableIRQ( this->irq );
        // Chip_UART_DeInit( this->usart );
        Chip_Clock_DisablePeriphClock( SYSCTL_CLOCK_UART0 );
    }

    // ----------------------------------------------------------------------------
    //   LOW-LEVEL RX/TX FUNCTIONS, ALSO USED IN IRQ HANDLER
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    /*
     * When this returns true, that means UART is ready for the next byte to be sent.
     * This usually means: the top byte of the hardware UART FIFO is available
     * to accept next byte to be sent.
     */
    bool UARTContext::TxRegisterEmpty()
    {
        return ( Chip_UART_ReadLineStatus( this->usart ) & UART_LSR_THRE ) != 0;
    }

    bool UARTContext::TxRBREmpty()
    {
        return this->tx_rbr->isEmpty();
    }

    // ----------------------------------------------------------------------------
    /*
     * When this returns true, that means UART has received a byte and it is waiting
     * for us to read it. This usually means: location, at which UART register that
     * points to the oldest received byte in hardware UART FIFO, has not been read yet.
     * There's still data to read from receive FIFO.
     */
    bool UARTContext::RxRegisterHasData()
    {
        return ( Chip_UART_ReadLineStatus( this->usart ) & UART_LSR_RDR ) != 0;
    }

    // ----------------------------------------------------------------------------
    // TX/RX IRQ handlers
    // ----------------------------------------------------------------------------
    /* UART transmit-only interrupt handler for ring buffers */
    void UARTContext::Transmit()
    {
        uint8_t ch;

        // Fill FIFO until full or until TX ring buffer is empty
        while( this->TxRegisterEmpty() && this->tx_rbr->popOne( &ch ) )
        {
            Chip_UART_SendByte( this->usart, ch );
        }
    }

    /* UART receive-only interrupt handler for ring buffers */
    void UARTContext::Receive()
    {
        /* New data will be ignored if data not popped in time */
        while( this->RxRegisterHasData() )
        {
            uint8_t ch = Chip_UART_ReadByte( this->usart );
            this->rx_rbr->pushOne( &ch );
        }
    }
    // ----------------------------------------------------------------------------


    // ----------------------------------------------------------------------------
    void UARTContext::DisableTHRInterrupt()
    {
        Chip_UART_IntDisable( this->usart, UART_IER_THREINT );
    }

    // ----------------------------------------------------------------------------
    void UARTContext::EnableTHRInterrupt()
    {
        Chip_UART_IntEnable( this->usart, UART_IER_THREINT );
    }

    // ----------------------------------------------------------------------------
    uint32_t UARTContext::THRInterruptEnabled()
    {
        return this->usart->IER & UART_IER_THREINT;
    }

} // namespace


// ----------------------------------------------------------------------------
//   STANDARD SOC DEVICE: SERIAL
// ----------------------------------------------------------------------------

// LPC1347 has only one UART so we fix things here
static serial::UARTContext UART0Context( 0 );

serial::Serial Serial( ( serial::hal::UART* ) &UART0Context );


// ----------------------------------------------------------------------------
//   UART IRQ HANDLER
// ----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

/**
 * All LPC IRQ handlers and such have to be implemented here
 * within extern "C" declaration, otherwise their names will get mangled
 * by C++ compiler and they WON'T get linked where you would expect them
 * to be because they are declared with __attribute__((weak)).
 */

/**
 * Each USART peripheral will have it's own IRQ handler.
 * In the IRQ code, use a corresponding UARTContext declared above,
 * similar to what we did here for UART0 (the only USART on LPC1347)
 */
void UART_IRQHandler( void )
{
    /* Want to handle any errors? Do it here. */

    /* Use default ring buffer handler. Override this with your own
       code if you need more capability. */
    /* Handle transmit interrupt if enabled */
    if( UART0Context.THRInterruptEnabled() )
    {
        UART0Context.Transmit();

        /* Disable transmit interrupt if the ring buffer is empty */
        if( UART0Context.TxRBREmpty() )
        {
            UART0Context.DisableTHRInterrupt();
        }
    }

    /* Handle receive interrupt */
    UART0Context.Receive();
}


#ifdef __cplusplus
}
#endif
