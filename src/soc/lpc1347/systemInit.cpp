// zcode
#include <zcore>
#include <soc/chip_lpc1347>

// lpc_open
#include "chip.h"


#ifdef __cplusplus
extern "C" {
#endif

/**
 * All LPC IRQ handlers and such have to be implemented
 * within extern "C" declaration, otherwise their names will get mangled
 * by C++ compiler and they WON'T get linked where you would expect them
 * to be because they are declared with __attribute__((weak)).
 */


/**
 * @brief   Handle interrupt from SysTick timer
 * @return  Nothing
 */
void SysTick_Handler(void)
{
    system::SystemTickHandler();
}

void lowLevelSystemInit( void )
{
    soc::initialize();

    board::setupClocking();

    gpio::hal::initialize();
    board::setupPins();

    board::initialize();

    /*
    initialize peripherals that are expected
    to be present by Zeeduino core:
    serial, adc, pwm, delay etc.
    i.e. these are supposed to exist for any board variant
    */

    // This has to be called sometime after board::setupClocking()
    // Set systick timer to 1ms interval
    SysTick_Config(SystemCoreClock / 1000);

    serial::hal::initialize();
    adc::hal::initialize();
    spi::hal::initialize();

    // Board_Delay_InitTimer();
    // Board_ADC_Init();
    // Board_PWM_Init_CT32B0();
    // Board_PWM_Init_CT16B0();
    // Board_PWM_Init_CT16B1();
    // Serial_Init();
}

#ifdef __cplusplus
}
#endif
