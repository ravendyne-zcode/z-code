// zcode
#include <zcore>
#include <soc/chip_lpc1347>

// lpc_open
#include "chip.h"

namespace adc
{
    const float hal::Vref = 3.3; // same as Vcc: 3.3V
    const uint16_t hal::maxValue = 0x0FFF; // 12-bit precision

    static uint8_t getClkDiv( uint32_t adcRate )
    {
        // The APB clock ( PCLK_ADC0 ) is divided by ( CLKDIV + 1 ) to produce the clock for
        // A/D converter, which should be less than or equal to 4.5MHz.
        // A fully conversion requires ( bits_accuracy + 1 ) of these clocks.
        // ADC Clock = PCLK_ADC0 / ( CLKDIV + 1 );
        // ADC rate = ADC clock / ( the number of clocks required for each conversion );

        uint32_t adcBlockFreq = Chip_Clock_GetSystemClockRate();

        // 31 - Number of clocks for a full conversion on LPC1347
        uint32_t fullAdcRate = adcRate * 31;

        /* Get the round value by fomular: (2*A + B)/(2*B) */
        return ( ( adcBlockFreq * 2 + fullAdcRate ) / ( fullAdcRate * 2 ) ) - 1;
    }

    void hal::initialize()
    {
        // ADC pin multiplexing is set up in board::initPins()

        uint32_t cr = 0;

        Chip_SYSCTL_PowerUp( SYSCTL_POWERDOWN_ADC_PD );

        Chip_Clock_EnablePeriphClock( SYSCTL_CLOCK_ADC );

        LPC_ADC->ADTRM = 0xF00;

        // Disable all ADC interrupts
        LPC_ADC->INTEN = 0;

        cr |= ADC_CR_LPWRMODE;

        // LPC1347
        #define ADC_MAX_SAMPLE_RATE 500000
        cr |= ADC_CR_CLKDIV( getClkDiv( ADC_MAX_SAMPLE_RATE ) );

        LPC_ADC->CR = cr;
    }


    class ADCContext : public hal::Adc
    {
    public:
        ADCContext( ADC_CHANNEL_T adcChannel, int halPinNumber );
        ADCContext( ADC_CHANNEL_T adcChannel, int socPortNumber, int socPinNumber );

        virtual uint16_t read();

    private:
        LPC_ADC_T* adc;
        ADC_CHANNEL_T adcChannel;
    };

    ADCContext::ADCContext( ADC_CHANNEL_T adcChannel, int halPinNumber )
    {
        this->adc = LPC_ADC;
        this->adcChannel = adcChannel;
    }

    ADCContext::ADCContext( ADC_CHANNEL_T adcChannel, int socPortNumber, int socPinNumber )
    {
        this->adc = LPC_ADC;
        this->adcChannel = adcChannel;
    }

    uint16_t ADCContext::read()
    {
        uint16_t dataADC;

        // Start ADC conversion now
        Chip_ADC_SetStartMode( this->adc, ADC_START_NOW, ADC_TRIGGERMODE_RISING );

        // Wait for conversion to complete
        while( Chip_ADC_ReadStatus( this->adc, this->adcChannel, ADC_DR_DONE_STAT ) != SET ) {}

        Chip_ADC_ReadValue( this->adc, this->adcChannel, &dataADC );

        return dataADC;
    }
}

// ----------------------------------------------------------------------------
//   STANDARD SOC DEVICE: ADC
// ----------------------------------------------------------------------------

static adc::ADCContext ADC0Context( ADC_CH0, -1 );
static adc::ADCContext ADC1Context( ADC_CH1, -1 );
static adc::ADCContext ADC2Context( ADC_CH2, -1 );
static adc::ADCContext ADC3Context( ADC_CH3, -1 );
static adc::ADCContext ADC4Context( ADC_CH5, -1 );
static adc::ADCContext ADC5Context( ADC_CH7, -1 );

adc::Adc AD0( ( adc::hal::Adc* ) &ADC0Context );
adc::Adc AD1( ( adc::hal::Adc* ) &ADC1Context );
adc::Adc AD2( ( adc::hal::Adc* ) &ADC2Context );
adc::Adc AD3( ( adc::hal::Adc* ) &ADC3Context );
adc::Adc AD4( ( adc::hal::Adc* ) &ADC4Context );
adc::Adc AD5( ( adc::hal::Adc* ) &ADC5Context );
