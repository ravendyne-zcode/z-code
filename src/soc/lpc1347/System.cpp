// zcode
#include <zcore>
#include <soc/chip_lpc1347>

// lpc_open
#include "chip.h"


namespace system
{

    void yield()
    {
        __WFI();
    }

}
