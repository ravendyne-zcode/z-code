/*
To play with:
https://www.adafruit.com/product/1897
https://www.adafruit.com/product/1455
*/

#if 1
// zcode
#include <zcore>
#include <soc/chip_lpc1347>
#include <util/RingBuffer.hpp>

#include <compiler/section_macros.hpp>

// lpc_open
#include "chip.h"


namespace spi
{
    void hal::initialize()
    {
        // TODO: move the SPI init things here
    }

    // This implementation is half-duplex: we can only send or receive data
    // That's why we have only one ring-buffer here.
    // Don't overlap Rx/Tx: start transmitting only after you have read all the data
    // and vice versa!!
    #define SPI_RBR_SIZE 64

    // Place SPI0 RX/TX buffer to LPC1347 peripheral RAM
    __SECTION_BSS_(PeripheralRam) uint8_t SPI1_rbr_buffer[ SPI_RBR_SIZE ];

    util::RingBuffer SPI1_rbr( SPI1_rbr_buffer, SPI_RBR_SIZE, sizeof( SPI1_rbr_buffer[ 0 ] ) );

    enum SPITransferState
    {
        Idle,
        Transmitting,
        Receiving,
    };

    class SPIContext : public hal::SPI
    {
    public:
        SPIContext( int spiDeviceNumber, util::RingBuffer* rbr, gpio::Pin* sselPin );

        void init( int spiDeviceNumber );

        bool write( uint8_t byte );
        bool read( uint8_t *byte );
        uint32_t write( const uint8_t *buffer, uint32_t count );
        uint32_t read( uint8_t *buffer, uint32_t count );

        void beginTransaction();
        void endTransaction();

        void interruptHandler();

    protected:
        void waitForTransferDone();

        void Start();
        void Stop();

        bool HasMoreToTransfer();
        uint32_t BytesToReceive();
        bool TransmitReceive();
        void DisableInterrupt();
        void EnableInterrupt();
        void DisableAllInterrupts();
        void FlushSspRxFifo();
        void WaitForSSPToFinish();

        void SetTransferCompleted();
        void SetStateTransmitting();
        void SetStateReceiving();
        bool TransferDone();

    private:
        SPITransferState transferState;
        bool flagCleanUpRxFifo;

        LPC_SSP_T* ssp;
        IRQn_Type irq;
        util::RingBuffer *rbr;
        gpio::Pin *sselPin;


        uint32_t bytesToReceive;

        void ReadFifo();
        void WriteFifo();
        void StartTransmitReceive();

        void SetSSPClockDivider( uint32_t div );
        void EnableSSPClock();
        void DisableSSPClock();
        void ResetSSP();
    };

    SPIContext::SPIContext( int spiDeviceNumber, util::RingBuffer* rbr, gpio::Pin* sselPin )
    {
        this->rbr = rbr;
        this->bytesToReceive = 0;
        this->sselPin = sselPin;
        this->sselPin->direction( PinDir::Output );
        this->sselPin->high();

        // state -> Idle
        this->SetTransferCompleted();
        flagCleanUpRxFifo = true;

        this->init( spiDeviceNumber );
    }

// ----------------------------------------------------------------------------

    void SPIContext::WaitForSSPToFinish()
    {
        // wait for any previous transfers to finish
        while( SET == Chip_SSP_GetStatus( this->ssp, SSP_STAT_BSY ) )
        {
//            system::yield();
        }
    }

    void SPIContext::ResetSSP()
    {
        if( LPC_SSP0 == this->ssp )
        {
            Chip_SYSCTL_PeriphReset( RESET_SSP0 );
        }
        else
        {
            Chip_SYSCTL_PeriphReset( RESET_SSP1 );
        }
    }

    void SPIContext::EnableSSPClock()
    {
        if( LPC_SSP0 == this->ssp )
        {
            Chip_Clock_EnablePeriphClock( SYSCTL_CLOCK_SSP0 );
        }
        else
        {
            Chip_Clock_EnablePeriphClock( SYSCTL_CLOCK_SSP1 );
        }
    }

    void SPIContext::DisableSSPClock()
    {
        if( LPC_SSP0 == this->ssp )
        {
            Chip_Clock_DisablePeriphClock( SYSCTL_CLOCK_SSP0 );
        }
        else
        {
            Chip_Clock_DisablePeriphClock( SYSCTL_CLOCK_SSP1 );
        }
    }

    void SPIContext::SetSSPClockDivider( uint32_t div )
    {
        if( LPC_SSP0 == this->ssp )
        {
            Chip_Clock_SetSSP0ClockDiv( div );
        }
        else
        {
            Chip_Clock_SetSSP1ClockDiv( div );
        }
    }

    void SPIContext::init( int spiDeviceNumber )
    {
        if( 0 == spiDeviceNumber )
        {
            this->ssp = LPC_SSP0;
            this->irq = SSP0_IRQn;
        }
        else
        {
            this->ssp = LPC_SSP1;
            this->irq = SSP1_IRQn;
        }

        // SPI pins are initialized in board specifc pins related source

        Chip_SSP_Disable( this->ssp );

        this->EnableSSPClock();
        this->SetSSPClockDivider( 1 );
        this->ResetSSP();
        // all interrupts should be disabled after reset anyways
        this->DisableAllInterrupts();

        Chip_SSP_Set_Mode( this->ssp, SSP_MODE_MASTER );
//        Chip_SSP_SetFormat( this->ssp, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL1 );
        Chip_SSP_SetFormat( this->ssp, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA0_CPOL0 );
        Chip_SSP_SetBitRate( this->ssp, 100000 );

        this->FlushSspRxFifo();
        this->rbr->flush();

        this->Start();
    }

    void SPIContext::waitForTransferDone()
    {
        // wait for our internal ring-buffer to be completely transferred to/from SSP FIFO
        while( ! this->TransferDone() )
        {
//            system::yield();
        }
        // at this point:
        // state -> Idle
    }

    void SPIContext::Start()
    {
        Chip_SSP_Enable( this->ssp );

        /* preemption = 1, sub-priority = 1 */
        NVIC_SetPriority( this->irq, 1 );
        NVIC_EnableIRQ( this->irq );
    }

    void SPIContext::Stop()
    {
        NVIC_DisableIRQ( this->irq );

        Chip_SSP_Disable( this->ssp );

        this->DisableSSPClock();

        this->SetSSPClockDivider( 0 );
    }

    // Main task of this method is to make sure that SSEL pin
    // transitions from HIGH to LOW state before the method finishes.
    //
    // So, we first need to make sure that SSEL is HIGH, i.e.
    // slave is not selected, before we begin with read/writes, and then,
    // once SSEL is HIGH, either this method will drive it LOW
    // before it returns or the first read/write to SPI FIFO will do that,
    // which depends on implementation.
    void SPIContext::beginTransaction()
    {
        this->waitForTransferDone();
        // at this point:
        // state -> Idle

        this->WaitForSSPToFinish();

        // now the SSEL should be in HIGH state (slave not selected)
        // and we can start with the next rx/tx session

        // make it a clean start, just in case
        this->FlushSspRxFifo();
        this->rbr->flush();

        // due to the way we implemented read/write operations
        // (each r/w op empties SSP Tx FIFO which for the SSP controller means "drive SSEL HIGH",
        // and we don't want to have that before endTransaction() has been called)
        // we have to manage SSEL pin manually
        this->sselPin->low();
    }

    // Main task of this method is to make sure SSEL pin
    // transitions from LOW to HIGH state before the method finishes.
    //
    // So, we first need to make sure that any transfer that may be
    // in progress has been finished, and then we should either drive
    // SSEL pin HIGH here or it will automatically be set to HIGH by SSP once
    // TX FIFO has been emptied, which depends on implementation.
    void SPIContext::endTransaction()
    {
        this->waitForTransferDone();

        // at this point:
        // state -> Idle

        this->WaitForSSPToFinish();

        // due to the way we implemented read/write operations
        // (each r/w op empties SSP Tx FIFO which for the SSP controller means "drive SSEL HIGH",
        // and we don't want to have that before endTransaction() has been called)
        // we have to manage SSEL pin manually
        this->sselPin->high();
    }

    // This method will transmit one byte
    // and discard any data received during that transmission
    // Can only be triggered from Idle state.
    bool SPIContext::write( uint8_t byte )
    {
        this->waitForTransferDone();

        // at this point:
        // state -> Idle

        // disable "Tx FIFO half-empty" interrupt
        this->DisableInterrupt();

        this->rbr->flush();
        this->rbr->pushOne( &byte );

        // do the initial transfer here in order to trigger further interrupts
        this->SetStateTransmitting();

        // at this point:
        // state -> Transmitting

        this->TransmitReceive();
        // enable "Tx FIFO half-empty" interrupt
        this->EnableInterrupt();

        // if call to read() follows this method,
        // signal to it that it needs to discard any
        // data in SSP Rx FIFO.
        this->flagCleanUpRxFifo = true;

        return true;
    }

    // This method will receive one byte
    // by transmitting one dummy byte to the slave device.
    // Can only be triggered from Idle state.
    bool SPIContext::read( uint8_t *byte )
    {
        this->waitForTransferDone();

        // we have to wait for SSP to finish whatever it is doing
        // so we can be sure that the bytes we read are the ones
        // we should actually read.
        this->WaitForSSPToFinish();

        // at this point:
        // state -> Idle

        if( this->flagCleanUpRxFifo )
        {
            // cleanup SSP Rx FIFO

            // this flag is set when we are transmitting
            // which also receives data at the same time, but we
            // need to discard that data, since we are doing
            // half-duplex thingie here

            // the state == Idle here, so calling ReadFifo()
            // will not push Rx bytes to our internal ring-buffer
            while( Chip_SSP_GetStatus( this->ssp, SSP_STAT_RNE ) == SET )
            {
                this->ReadFifo();
            }

            // job done, flag it
            this->flagCleanUpRxFifo = false;
        }

        if( this->rbr->popOne( byte ) == 0 )
        {
            // no data in RX buffer, initiate receiving
            this->bytesToReceive = 1;
            this->SetStateReceiving();

            // at this point:
            // state -> Receiving

            this->TransmitReceive();
            // enable "Tx FIFO half-empty" interrupt
            this->EnableInterrupt();

            this->waitForTransferDone();

            // at this point:
            // state -> Idle

            this->rbr->popOne( byte );
        }

        return true;
    }

    uint32_t SPIContext::write( const uint8_t *buffer, uint32_t count )
    {
        this->waitForTransferDone();

        // at this point:
        // state -> Idle

        // disable "Tx FIFO half-empty" interrupt
        this->DisableInterrupt();

        this->rbr->flush();
        count = this->rbr->push( buffer, count );

        // do the initial transfer here in order to trigger further interrupts
        this->SetStateTransmitting();

        // at this point:
        // state -> Transmitting

        this->TransmitReceive();
        // enable "Tx FIFO half-empty" interrupt
        this->EnableInterrupt();

        // if call to read() follows this method,
        // signal to it that it needs to discard any
        // data in SSP Rx FIFO.
        this->flagCleanUpRxFifo = true;

        return count;
    }

    uint32_t SPIContext::read( uint8_t *buffer, uint32_t count )
    {
        this->waitForTransferDone();

        // we have to wait for SSP to finish whatever it is doing
        // so we can be sure that the bytes we read are the ones
        // we should actually read.
        this->WaitForSSPToFinish();

        // at this point:
        // state -> Idle

        if( this->flagCleanUpRxFifo )
        {
            // cleanup SSP Rx FIFO

            // this flag is set when we are transmitting
            // which also receives data at the same time, but we
            // need to discard that data, since we are doing
            // half-duplex thingie here

            // the state == Idle here, so calling ReadFifo()
            // will not push Rx bytes to our internal ring-buffer
            while( Chip_SSP_GetStatus( this->ssp, SSP_STAT_RNE ) == SET )
            {
                this->ReadFifo();
            }

            // job done, flag it
            this->flagCleanUpRxFifo = false;
        }

        uint32_t bytesRead = this->rbr->pop( buffer, count );
        if( bytesRead < count )
        {
            count -= bytesRead;

            // we need more data to read than was available in ring-buffer
            this->bytesToReceive = count;
            this->SetStateReceiving();

            // at this point:
            // state -> Receiving

            this->TransmitReceive();
            // enable "Tx FIFO half-empty" interrupt
            this->EnableInterrupt();

            this->waitForTransferDone();

            // at this point:
            // state -> Idle

            bytesRead += this->rbr->pop( buffer + bytesRead, count );
        }

        return bytesRead;
    }

// ----------------------------------------------------------------------------

    bool SPIContext::HasMoreToTransfer()
    {
        return SPITransferState::Transmitting == this->transferState && ! this->rbr->isEmpty();
    }

    void SPIContext::FlushSspRxFifo()
    {
        // Clean up SSP RX FIFO
        Chip_SSP_Int_FlushData( this->ssp );
    }

    void SPIContext::SetTransferCompleted()
    {
        this->transferState = SPITransferState::Idle;
    }

    void SPIContext::SetStateTransmitting()
    {
        this->transferState = SPITransferState::Transmitting;
    }

    void SPIContext::SetStateReceiving()
    {
        this->transferState = SPITransferState::Receiving;
    }

    // this means there are no data to be sent from ring-buffer
    // or received from SPI FIFO. When this returns true,
    // the SPI peripheral might still have data in its TX FIFO
    // and might be busy sending them out
    bool SPIContext::TransferDone()
    {
        return SPITransferState::Idle == this->transferState;
    }

    uint32_t SPIContext::BytesToReceive()
    {
        return this->bytesToReceive;
    }

// ----------------------------------------------------------------------------

    // This will ALWAYS read one byte from SPI RX FIFO
    // The byte will be placed into ring-buffer ONLY if we are receiving
    void SPIContext::ReadFifo()
    {
        // get one SSP frame, which is always 16-bit
        // but in this case only lower 8-bits are valid
        // since we have set up SSP to work in 8-bit mode
        uint8_t byte = (uint8_t) Chip_SSP_ReceiveFrame( this->ssp );

        // If we are receiving and we are still expecting more bytes to come in,
        // store the read byte into our ring-buffer
        if( this->transferState == SPITransferState::Receiving && this->bytesToReceive > 0 )
        {
            this->rbr->pushOne( &byte );
            this->bytesToReceive--;
        }
    }

    // This will ALWAYS write one byte to SPI TX FIFO
    // The byte will come from ring-buffer ONLY if we are transmitting
    void SPIContext::WriteFifo()
    {
        uint8_t byte = 0xFF;

        // If we are transmitting and there's data in the ring-buffer, get the next byte to send
        if( this->transferState == SPITransferState::Transmitting && ! this->rbr->isEmpty() )
        {
            this->rbr->popOne( &byte );
        }

        // If we are not transmitting, this will send a dummy byte out
        // which is needed in order to receive anything via SPI
        Chip_SSP_SendFrame( this->ssp, byte );
    }

    // Transmit/receive as much bytes as possible during this round.
    // Called in SSP interrupt handler, except when initiating read/write operations.
    // Calling this outside of interruptHandler or read/write may result in unexpected
    // results. Or something like that.
    bool SPIContext::TransmitReceive()
    {
        switch( this->transferState )
        {
            // Transimt
            //
            // aim to top up SSP TX FIFO by emptying our ring-buffer into it.
            case SPITransferState::Transmitting:
            {

                // while SSP TX FIFO is not full ( TNF == 1 )
                // and there's data in our TX buffer to be sent
                while( (Chip_SSP_GetStatus( this->ssp, SSP_STAT_TNF ) == SET) && ! this->rbr->isEmpty() )
                {
                    // write one data byte to SSP
                    this->WriteFifo();

                    // no need to read data from SPI RX FIFO
                    // any garbage will be discarded prior to the start of receiving phase
                }

                return true;

            }
            break;

            // Receive
            //
            // aim to empty out RX FIFO first and then to request as much reads from slave
            // as possible by filling up TX FIFO with dummy data.
            case SPITransferState::Receiving:
            {

                // while SSP RX FIFO is not empty ( RNE == 1 ) and we need more data
                while( (Chip_SSP_GetStatus( this->ssp, SSP_STAT_RNE ) == SET) && this->bytesToReceive > 0 )
                {
                    // read in data byte from SSP TX FIFO
                    this->ReadFifo();
                }

                // 'bytesToReceive' is decremented ONLY when we have placed a received byte into our
                // internal ring-buffer. Because of that we are using temp var here in order to know
                // how many dummy bytes to place into Tx FIFO.
                uint32_t maxBytesToRequest = this->bytesToReceive;
                // if we still expect something to receive, initiate slave reads by filling SPI TX FIFO with dummy data.
                // Write either as many dummies as there's space in TX FIFO
                // or as many as we still expect to receive, whichever is less.
                while( (Chip_SSP_GetStatus( this->ssp, SSP_STAT_TNF ) == SET) && maxBytesToRequest > 0 )
                {
                    // this will transmit one dummy byte,
                    // we have to do that in order to receive anything
                    this->WriteFifo();
                    maxBytesToRequest--;
                }

                return true;

            }
            break;

            default:
            	// so we have no warnings
            break;
        }

        return false;
    }

    // Main task of this method is to transfer as much bytes as possible from/to
    // internal ring-buffer to/from SSP Rx/Tx FIFOs.
    // This is executed every time SSP controller triggers "Tx FIFO half-empty" interrupt.
    // This is the place where SPI state transitions from Transmitting/Receiving to Idle.
    void SPIContext::interruptHandler()
    {
        // disable "Tx FIFO half-empty" interrupt
        this->DisableInterrupt();

        // send/receive as much as possible
        this->TransmitReceive();

        // wether transmitting or receaving, TransmitReceive() will fill in
        // SSP Tx FIFO with either data to be sent or with dummy byte used to
        // clock in the data we want to read.
        // In any case, we need to enable Tx "FIFO half-empty" interrupt if
        // there's more data to be sent/received
        if( this->HasMoreToTransfer() || this->BytesToReceive() > 0 )
        {
            // enable "Tx FIFO half-empty" interrupt
            this->EnableInterrupt();
        }
        // if we're done:
        // leave "Tx FIFO half-empty" interrupt disabled,
        // and signal SPI transfer completed
        else
        {
            this->SetTransferCompleted();
            // at this point:
            // state -> Idle
        }

    }

    // This disables SSP "Tx FIFO at least half empty" interrupt (SSP_TXIM)
    void SPIContext::DisableInterrupt()
    {
        Chip_SSP_Int_Disable( this->ssp );
    }

    // This enables SSP "Tx FIFO at least half empty" interrupt (SSP_TXIM)
    void SPIContext::EnableInterrupt()
    {
        Chip_SSP_Int_Enable( this->ssp );
    }

    void SPIContext::DisableAllInterrupts()
    {
        this->ssp->IMSC = 0x00;
    }

// ----------------------------------------------------------------------------

}


// ----------------------------------------------------------------------------
//   STANDARD SOC DEVICE: SPI
// ----------------------------------------------------------------------------

// We'll fix things here to SSP0 for now
// using pin_1 for SSEL
static gpio::Pin _SSEL( 1 );
static spi::SPIContext SPI1Context( 1, &spi::SPI1_rbr, &_SSEL );

spi::Spi Spi( ( spi::hal::SPI* ) &SPI1Context );


// ----------------------------------------------------------------------------
//   SSP IRQ HANDLER
// ----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

/**
 * All LPC IRQ handlers and such have to be implemented here
 * within extern "C" declaration, otherwise their names will get mangled
 * by C++ compiler and they WON'T get linked where you would expect them
 * to be because they are declared with __attribute__((weak)).
 */

/**
 * Each SSP peripheral will have it's own IRQ handler.
 * In the IRQ code, use a corresponding SPIContext declared above,
 * similar to what we did here for SSP0
 */
/*
void SSP0_IRQHandler(void)
{
}
*/
void SSP1_IRQHandler(void)
{
    // Since the only interrupt we have enabled is TXIM,
    // we don't need to check for the source of interrupt
    // by testing MIS register bits

    SPI1Context.interruptHandler();
}


#ifdef __cplusplus
}
#endif

#endif // 0
