// zcode
#include <zcore>
#include <soc/chip_lpc1347>

// lpc_open
#include "chip.h"

void gpio::hal::initialize()
{
    /* Enable IOCON clock */
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_IOCON);
}
