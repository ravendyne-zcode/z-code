// zcode
#include <zcore>
#include <soc/chip_lpc1347>

// lpc_open
#include "chip.h"

void soc::initialize()
{
    /* Booting from FLASH, so remap vector table to FLASH */
    Chip_SYSCTL_Map(REMAP_USER_FLASH_MODE);
}

void soc::powerDown(void)
{
    // enter power-down mode
}
