#if 0
// zcode
#include <zcore>
#include <soc/chip_lpc1347>

// lpc_open
#include "chip.h"


#define BB_DELAY() {}

namespace spi
{
    void hal::initialize()
    {
        // Nothing to see here, move on.
    }


    class SPIContextBitBang : public hal::SPI
    {
    public:
        SPIContextBitBang();

        void init();

        bool write( uint8_t byte );
        bool read( uint8_t *byte );
        uint32_t write( const uint8_t *buffer, int count );
        uint32_t read( uint8_t *buffer, int count );

        void beginTransaction();
        void endTransaction();

        uint8_t TransmitReceive_MSB( uint8_t data );

    private:
        gpio::hal::Pin *SSEL;
        gpio::hal::Pin *MISO;
        gpio::hal::Pin *MOSI;
        gpio::hal::Pin *SCK;

    };

    SPIContextBitBang::SPIContextBitBang()
    {
        this->init();
    }

// ----------------------------------------------------------------------------
    void SPIContextBitBang::init()
    {
        // These will set the pins to their default functions, which are NOT GPIO for these pins
        // So we call these here first and then set up all the pins to GPIO below with PinMuxSet()
        static gpio::hal::Pin _MOSI( 1, 22 );
        static gpio::hal::Pin _MISO( 1, 21 );
        static gpio::hal::Pin _SCK( 1, 20 );
        static gpio::hal::Pin _SSEL( 0, 17 );

        this->SSEL = &_SSEL;
        this->MOSI = &_MOSI;
        this->MISO = &_MISO;
        this->SCK = &_SCK;

        // Initialize SPI pins
        // this->MOSI->mode( PinMode::GPIO );
        Chip_IOCON_PinMuxSet( LPC_IOCON, this->MOSI->getPort(), this->MOSI->getPin(), ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) );
        // this->MISO->mode( PinMode::GPIO );
        Chip_IOCON_PinMuxSet( LPC_IOCON, this->MISO->getPort(), this->MISO->getPin(), ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) );
        // this->SCK->mode( PinMode::GPIO );
        Chip_IOCON_PinMuxSet( LPC_IOCON, this->SCK->getPort(), this->SCK->getPin(), ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) );
        // this->SSEL->mode( PinMode::GPIO );
        Chip_IOCON_PinMuxSet( LPC_IOCON, this->SSEL->getPort(), this->SSEL->getPin(), ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) );

        this->SSEL->direction( PinDir::Output );
        this->SSEL->high();

        this->MOSI->direction( PinDir::Output );
        this->MOSI->high();

        this->MISO->direction( PinDir::Input );

        this->SCK->direction( PinDir::Output );
        // CPOL0 -> clock is LOW on idle
        this->SCK->low();
    }

    // Main task of this method is to make sure that SSEL pin
    // transitions from HIGH to LOW state before the method finishes.
    void SPIContextBitBang::beginTransaction()
    {
        this->SSEL->low();
    }

    // Main task of this method is to make sure SSEL pin
    // transitions from LOW to HIGH state before the method finishes.
    void SPIContextBitBang::endTransaction()
    {
        this->SSEL->high();
    }

    uint8_t SPIContextBitBang::TransmitReceive_MSB( uint8_t data )
    {
        uint8_t rx;
        int i;

        BB_DELAY();

        rx = 0x00;
        for( i = 7; i >= 0; i-- )
        {
            rx <<= 1;

            this->SCK->low();
            BB_DELAY();

            // CPHA0 -> sample data on SCK transition from IDLE to ACTIVE
            if( data & (1 << i) )
            {
                this->MOSI->high();
            }
            else
            {
                this->MOSI->low();
            }
            BB_DELAY();

            this->SCK->high();
            BB_DELAY();

            if( this->MISO->read() )
            {
                rx |= 1;
            }

            BB_DELAY();
        }
        this->SCK->low();
        this->MOSI->low();

        return rx;
    }

    // This method will transmit one byte
    // and discard any data received during that transmission
    // Can only be triggered from Idle state.
    bool SPIContextBitBang::write( uint8_t byte )
    {
        this->TransmitReceive_MSB( byte );

        return true;
    }

    // This method will receive one byte
    // by transmitting one dummy byte to the slave device.
    // Can only be triggered from Idle state.
    bool SPIContextBitBang::read( uint8_t *byte )
    {
        *byte = this->TransmitReceive_MSB( 0xFF );

        return true;
    }

    uint32_t SPIContextBitBang::write( const uint8_t *buffer, int count )
    {
        int idx = 0;
        while( idx < count )
        {
            this->write( buffer[ idx ] );
            idx++;
        }

        return count;
    }

    uint32_t SPIContextBitBang::read( uint8_t *buffer, int count )
    {
        int idx = 0;
        while( idx < count )
        {
            this->read( buffer + idx );
            idx++;
        }

        return count;
    }

// ----------------------------------------------------------------------------

}


// ----------------------------------------------------------------------------
//   STANDARD SOC DEVICE: SPI
// ----------------------------------------------------------------------------

// We'll fix things here to SSP0 for now
// using pin_1 for SSEL
static spi::SPIContextBitBang SPIContextBB;

spi::Spi Spi( ( spi::hal::SPI* ) &SPIContextBB );

#endif
