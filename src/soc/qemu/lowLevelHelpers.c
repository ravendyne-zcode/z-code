#include <stdint.h>

void CPU_RESET()
{
    // http://infocenter.arm.com/help/topic/com.arm.doc.ddi0337e/DDI0337E_cortex_m3_r1p1_trm.pdf
    // 8-21: Application Interrupt and Reset Control Register

    // ARMv7m soft reset procedure
    // setting SYSRESETREQ bit in APINT register resets the system
    // if qemu is ran with -no-reboot this will effectivelly stop the darn thing
    volatile uint32_t * const SCB = (uint32_t *)(0xE000E000 + 0x0D00);
    volatile uint32_t * const APINT = SCB + 3;
    #define SYSRESETREQ (1<<2)
    #define VECTKEY (0x05FA << 16)
    uint32_t apint_v = VECTKEY + SYSRESETREQ;
    *APINT = apint_v;
}
