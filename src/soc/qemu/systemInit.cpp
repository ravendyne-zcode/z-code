#include <zcore>

#ifdef __cplusplus
extern "C" {
#endif

void SysTick_Handler(void)
{
}

void yield(void)
{
    // __WFI();
}

void lowLevelSystemInit( void )
{
}


#ifdef __cplusplus
}
#endif

extern "C" void CPU_RESET();
void soc::powerDown()
{
    // stops qemu VM
    CPU_RESET();
}
