#include <gpio>

namespace gpio
{
    // static Pin* pins[ MAX_PIN_COUNT ];
    static Pin::ChangeCallback cb;

    Pin::Pin( int socPinNumber )
    {
    }

    void Pin::high()
    {
        cb( 77 );
    }

    void Pin::low()
    {
        ;
    }

    void Pin::direction( PinDir direction )
    {
        ;
    }

    bool Pin::read()
    {
        return false;
    }

    void Pin::onChange( ChangeCallback callback )
    {
        cb = callback;
    }

}
