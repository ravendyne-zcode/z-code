// zcode
#include <serial>
#include <cstring>


namespace serial
{

    Serial::Serial( hal::UART* uart )
    {
        this->uart = uart;
    }

    bool Serial::write( char ch )
    {
        return this->uart->write( (uint8_t) ch );
    }

    void Serial::write( const char *string )
    {
        if( string == NULL )
        {
            return;
        }


        uint32_t charsLeft = std::strlen( string );

        while( charsLeft > 0 )
        {
            uint32_t charsWritten = this->uart->write( (uint8_t*) string, charsLeft );
            string += charsWritten;
            charsLeft -= charsWritten;
        }
    }

    char Serial::readChar()
    {
        char chr;
        this->uart->read( (uint8_t*) &chr );

        return chr;
    }

    int Serial::readLine( char *buffer, int maxLength )
    {
        return 0;
    }
}
