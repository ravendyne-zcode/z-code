// zcode
#include <core_soc>
#include <cstring>

#include <system>

namespace i2c
{

    I2c::I2c( hal::I2C* i2c )
    {
        this->state = I2c::State::NONE;
        this->i2c = i2c;

        this->currentDataIndex = 0;
        this->buffer = 0;
        this->transferType = I2c::TransferType::NONE;

        this->i2c->disableInterrupt();
        // construct delegate with a class member function pointer
        // hal::I2C::I2cEventCallback callback{ this, &I2c::onI2cEvent };
        // this->i2c->onEvent( callback );
        // or specify it directly as parameter
        this->i2c->onEvent( hal::I2C::I2cEventCallback { this, &I2c::onI2cEvent } );

        this->i2c->initialize();
//        this->i2c->master();
//        this->i2c->enableInterrupt();
    }

    void I2c::beginTransaction( uint16_t address )
    {
        this->address = address;
    }

    void I2c::endTransaction()
    {
        // Wait for all read/write to finish
        // wait for transfer to finish
        while( I2c::State::END != this->state &&
            I2c::State::ERROR != this->state &&
            I2c::State::ABORT != this->state )
        {
            // https://www.silabs.com/documents/public/application-notes/AN0039.pdf
            system::yield();
        }

        this->currentDataIndex = 0;
        this->transferType = I2c::TransferType::NONE;

        // or (TBD)
        // Register callback to be called when transaction is done
    }

    bool I2c::write( uint8_t byte )
    {
        util::ByteBuffer buffer( &byte, 1 );

        return this->write( buffer ) == 1;
    }

    bool I2c::read( uint8_t *byte )
    {
        util::ByteBuffer buffer( byte, 1 );

        return this->read( buffer ) == 1;
    }

    uint32_t I2c::write( util::ByteBuffer& byteBuffer )
    {
        this->buffer = &byteBuffer;
        this->beginWrite();

        return this->currentDataIndex;
    }

    uint32_t I2c::read( util::ByteBuffer& byteBuffer )
    {
        this->buffer = &byteBuffer;
        this->beginRead();

        return this->currentDataIndex;
    }


    void I2c::beginWrite()
    {
        this->transferType = I2c::TransferType::TX;
        this->begin();
    }

    void I2c::beginRead()
    {
        this->transferType = I2c::TransferType::RX;
        this->begin();
    }

    void I2c::begin()
    {
        this->state = I2c::State::BEGIN;
        // this should trigger onI2cEvent() by i2c hardware with START_SENT
//        this->i2c->master();

        //uint8_t address, uint8_t *txBuffer, uint16_t txSize, uint8_t *rxBuffer, uint16_t rxSize
        if( I2c::TransferType::TX == this->transferType )
        {
            this->i2c->transferBlocking(this->address, this->buffer->buffer, this->buffer->length, NULL, 0);
        }
        if( I2c::TransferType::RX == this->transferType )
        {
            this->i2c->transferBlocking(this->address, NULL, 0, this->buffer->buffer, this->buffer->length);
        }
//        this->i2c->start();
    }

    // Executed in IRQ context
    void I2c::onI2cEvent( hal::Event event )
    {
        switch( event )
        {
            case i2c::hal::Event::START_SENT:
                if ( I2c::State::BEGIN == this->state )
                {
                    this->i2c->address( address );
                    if( I2c::TransferType::TX == this->transferType )
                    {
                        this->i2c->addressModeWrite();
                    }
                    else if( I2c::TransferType::RX == this->transferType )
                    {
                        this->i2c->addressModeRead();
                    }
                    // this should trigger event DATA_SENT_(N)ACK_RECEIVED by i2c hardware
                    this->i2c->writeAddress();

                    this->state = I2c::State::ADDRESSING;
                }
                else
                {
                    this->state = I2c::State::ERROR;
                }
            break;

            case i2c::hal::Event::DATA_RECEIVED:
                if ( I2c::State::RECEIVING == this->state )
                {
                    if( this->currentDataIndex < buffer->length )
                    {
                        // this is ACK for the next byte to be received
                        this->i2c->ack();

                        this->i2c->read( &(*buffer)[ this->currentDataIndex ] );
                        ++this->currentDataIndex;

                        // handle nack and re-start/stop cases

                        // case: next-to-last byte received
                        // it has a special treatment so we can correctly generate
                        // nack and re-start/stop for the last byte
                        if( buffer->length - this->currentDataIndex == 1 )
                        {
                            // this is NACK for the next (last) byte to be received
                            this->i2c->nack();
                        }
                        else if( buffer->length == this->currentDataIndex )
                        {
                            // we received the last byte
                            this->i2c->nack();
                            this->i2c->stop();
                            this->state = I2c::State::END;
                        }
                    }
                    else
                    {
                        // we are past rx buffer end and still in State::RECEIVING
                        // must be an error, eh?
                        this->state = I2c::State::ERROR;
                    }
                }
                else
                {
                    this->state = I2c::State::ERROR;
                }
            break;

            case i2c::hal::Event::DATA_SENT_ACK_RECEIVED:
                if ( I2c::State::ADDRESSING == this->state )
                {
                    this->currentDataIndex = 0;

                    if( I2c::TransferType::TX == this->transferType )
                    {
                        this->state = I2c::State::SENDING;
                        // call us again to send the first data byte
                        this->onI2cEvent( i2c::hal::Event::DATA_SENT_ACK_RECEIVED );
                    }
                    else if( I2c::TransferType::RX == this->transferType )
                    {
                        this->state = I2c::State::RECEIVING;
                        // do nothing else in this case, DATA_RECEIVED event
                        // will be fired once the data is received by I2C hardware
                    }
                }
                else if ( I2c::State::SENDING == this->state )
                {
                    if( this->currentDataIndex < buffer->length )
                    {
                        // this should trigger event DATA_SENT_(N)ACK_RECEIVED by i2c hardware
                        this->i2c->write( (*buffer)[ this->currentDataIndex ] );
                        ++this->currentDataIndex;
                    }
                    else
                    {
                        this->i2c->stop();
                        this->state = I2c::State::END;
                    }
                }
                else
                {
                    this->state = I2c::State::ERROR;
                }
            break;

            case i2c::hal::Event::DATA_SENT_NACK_RECEIVED:
                // device doesn't respond to address or
                // device can't receive more data
                this->i2c->stop();

            case i2c::hal::Event::ARBITRATION_LOST:
            case i2c::hal::Event::BUS_ERROR:
            default:
                this->state = I2c::State::ABORT;

        }
    }

}

