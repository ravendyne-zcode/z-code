#include <streams>


namespace zcore
{

    long InputStream::skip( long n )
    {
        uint8_t dummy;

        long cnt = n;
        while( cnt > 0 )
        {
            this->read( &dummy );
            cnt--;
        }

        return n;
    }

    int InputStream::read( util::ByteBuffer &buffer )
    {
        return this->read( buffer.buffer, buffer.length );
    }

    int InputStream::read( uint8_t* buffer, int length )
    {
        uint8_t dummy;

        int idx = 0;
        while( idx < length )
        {
            buffer[ idx ] = this->read( &dummy );
            idx++;
        }

        return idx;
    }

    int OutputStream::write( util::ByteBuffer &buffer )
    {
        return this->write( buffer.buffer, buffer.length );
    }

    int OutputStream::write( uint8_t* buffer, int length )
    {
        int idx = 0;
        while( idx < length )
        {
            this->write( buffer[ idx ] );
            idx++;
        }

        return idx;
    }


}
