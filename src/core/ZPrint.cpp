#include <climits> // CHAR_BIT

#include <zprint>

ZPrint::ZPrint()
{
}

// This is the main workhose of the class
// It calls virtual write()
size_t ZPrint::write(uint8_t *source, size_t length)
{
    size_t n = length;
    while(n > 0)
    {
        write(*source);
        source++;
        n--;
    }
    return length;
}

size_t ZPrint::write(const char *source)
{
    size_t n = 0;
    while(*source != '\0')
    {
        write((uint8_t)*source);
        source++;
        n++;
    }
    return n;
}
size_t ZPrint::write(const char *source, size_t length)
{
    size_t n = length;
    while(*source != '\0' && n > 0)
    {
        write((uint8_t)*source);
        source++;
        n--;
    }
    return length - n;
}


size_t ZPrint::print(char c)
{
    return write((uint8_t)c);
}
size_t ZPrint::print(const char *text)
{
    return write(text);
}
size_t ZPrint::print(const zcore::String &text)
{
    // TODO
    return 0;
}

size_t ZPrint::print(int value, ConversionBase base=DEC)
{
    return print((int32_t)value, base);
}

size_t ZPrint::print(uint8_t value, ConversionBase base=DEC)
{
    return print((uint32_t)value, base);
}

size_t ZPrint::print(int16_t value, ConversionBase base=DEC)
{
    return print((int32_t)value, base);
}
size_t ZPrint::print(uint16_t value, ConversionBase base=DEC)
{
    return print((uint32_t)value, base);
}
size_t ZPrint::print(int32_t value, ConversionBase base=DEC)
{
    size_t n = 0;
    if (base == DEC && value < 0)
    {
        value = -value;
        n = print('-');
    }
    return n + print((uint32_t)value, base);
}
size_t ZPrint::print(uint32_t value, ConversionBase base=DEC)
{
    return convertWithBaseAndPrint(value, base);
}

size_t ZPrint::print(double value, int decimalPlaces=2)
{
    // TODO
    return 0;
}


size_t ZPrint::println()
{
    return write('\n');
}
size_t ZPrint::println(char c)
{
    return print(c) + println();
}
size_t ZPrint::println(const char *text)
{
    return print(text) + println();
}
size_t ZPrint::println(const zcore::String &text)
{
    return print(text) + println();
}
size_t ZPrint::println(uint8_t value, ConversionBase base=DEC)
{
    return print(value, base) + println();
}
size_t ZPrint::println(int16_t value, ConversionBase base=DEC)
{
    return print(value, base) + println();
}
size_t ZPrint::println(uint16_t value, ConversionBase base=DEC)
{
    return print(value, base) + println();
}
size_t ZPrint::println(int32_t value, ConversionBase base=DEC)
{
    return print(value, base) + println();
}
size_t ZPrint::println(uint32_t value, ConversionBase base=DEC)
{
    return print(value, base) + println();
}
size_t ZPrint::println(int value, ConversionBase base=DEC)
{
    return print((int32_t)value, base) + println();
}
size_t ZPrint::println(double value, int decimalPlaces=2)
{
    return print(value, decimalPlaces) + println();
}

// based on:
// http://interactivepython.org/runestone/static/pythonds/BasicDS/ConvertingDecimalNumberstoBinaryNumbers.html
// http://www.cs.trincoll.edu/~ram/cpsc110/inclass/conversions.html
static char digits[] =
{
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9' ,'A', 'B', 'C', 'D', 'E', 'F'
};

size_t ZPrint::convertWithBaseAndPrint(uint32_t value, ConversionBase base)
{
    uint32_t b = base;
    uint32_t n = value;
    // max size of result is for case when we convert to binary
    // we add 1 for trailing '\0' character
    // CHAR_BIT is number of bits in char type from <climits>
    size_t bufMaxSize = CHAR_BIT * sizeof(uint32_t) + 1;
    char buffer[bufMaxSize];

    // we are starting from right side and going to the left
    char *str = buffer + bufMaxSize - 1;

    // terminate with '\0'
    *str = '\0';

    do
    {
        str--;
        *str = digits[n % b];
        n = n / b;
    } while(n > 0);

    return write(str);
}

// http://www.cs.tufts.edu/~nr/cs257/archive/florian-loitsch/printf.pdf
// http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.72.4656&rep=rep1&type=pdf
// http://www.ryanjuckett.com/programming/printing-floating-point-numbers/
// but the simplest, and very restrictive, is:
// http://www.geeksforgeeks.org/convert-floating-point-number-string/
