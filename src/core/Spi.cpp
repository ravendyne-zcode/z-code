// zcode
#include <core_soc>
#include <cstring>

namespace spi
{

    Spi::Spi( hal::SPI* spi )
    {
        this->spi = spi;
    }

    void Spi::beginTransaction()
    {
        this->spi->beginTransaction();
    }

    void Spi::endTransaction()
    {
        this->spi->endTransaction();
    }

    bool Spi::write( uint8_t byte )
    {
        return this->spi->write( byte );
    }

    bool Spi::read( uint8_t *byte )
    {
        return this->spi->read( byte );
    }

    void Spi::write( const uint8_t *buffer, uint32_t length )
    {
        this->spi->write( buffer, length );
    }

    uint32_t Spi::read( uint8_t *buffer, uint32_t maxLength )
    {
        return this->spi->read( buffer, maxLength );
    }

}

