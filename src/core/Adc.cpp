// zcode
#include <core_soc>


namespace adc
{
    static const float quanta = hal::Vref / hal::maxValue;

    Adc::Adc( hal::Adc* adc )
    {
        this->adc = adc;
    }

    uint16_t Adc::readRaw()
    {
        return this->adc->read();
    }

    float Adc::value()
    {
        uint16_t value = this->adc->read();

        return quanta * value;
    }

}
