// zcode
#include <gpio>


namespace gpio
{

    Pin::Pin( uint32_t halPinNumber ) : hal::Pin::Pin( halPinNumber )
    {
    }

    bool Pin::isHigh()
    {
        return this->read();
    }

    bool Pin::isLow()
    {
        return ( ! this->read() );
    }

    void Pin::onChange( ChangeCallback callback )
    {
        callback( 0 );
    }

}
