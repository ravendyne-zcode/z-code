// zcode
#include <zcore>


namespace system
{

    void SystemTickHandler()
    {
        static int tickCount = 0;

        tickCount++;
        if( tickCount > 500 )
        {
            LED.toggle();
            tickCount = 0;
        }
    }

}
