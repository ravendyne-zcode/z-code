#include <zprint>
#include <zcore>

#include <util/Utils.hpp>

#define VERSION "v1.0.71"

#define TO_HEX(i) (i <= 9 ? '0' + i : 'A' - 10 + i)

char* byteToHex( uint8_t byte )
{
    static char res[3];

    res[0] = TO_HEX(((byte & 0xF0) >> 4));
    res[1] = TO_HEX((byte & 0x0F));
    res[2] = '\0';

    return res;
}

void testAT45DB161D()
{
    // gpio::Pin flashBusy = pin_2;
    gpio::Pin flashReset = pin_0;
    flashReset.direction( PinDir::Output );


    flashReset.low();
    // wait for it...
    for(int z=0; z<0x000F0000;) z++;
    flashReset.high();

    // make sure flash is ready
    // flash is busy if BUSY# == LOW
//    while( ! flashBusy.read() )
//    {
//        system::yield();
//    }

    // we need to control SSEL (for now)
    // so we'll just use our own SSEL
/*
    AT45DB161D requires the use of either
    SSP_CLOCK_CPHA1_CPOL1 or SSP_CLOCK_CPHA0_CPOL1
    in order for SSEL to be held low by LPC1347 during back-to-back data transmission.
    however, the flash supports modes 0 and 3, so we should set SSP
    to SSP_CLOCK_CPHA1_CPOL1.
    Or we could control SSEL on our own altogether, eh?

    For modes CPHA0_CPOL1 and CPHA1_CPOL1,
    LPC1347 SSP controller drives SSEL LOW once valid data is in the TX FIFO
    and keeps it LOW until TX FIFO is emptied at which point SSEL is
    returned to HIGH state.

    nice pictures of SPI modes
    http://dlnware.com/theory/SPI-Transfer-Modes
*/
    uint8_t manufacturerID = 0;
    // uint8_t deviceID[2] = {0};
    uint8_t deviceIDHi = 0;
    uint8_t deviceIDLo = 0;

    // give it some time...
    for(int z=0; z<0x000F0000;) z++;
    Spi.beginTransaction();
    // Manufacturer and Device ID read
    Spi.write( 0x9F );
    // read Manufacturer ID
    Spi.read( &manufacturerID );
    // read two byte Device ID
    // Spi.read( deviceID, 2 );
    Spi.read( &deviceIDHi );
    Spi.read( &deviceIDLo );
    Spi.endTransaction();

    // print the results
    Serial.write( "Manufacturer ID: 0x" );
    // expected: 1F
    Serial.write( byteToHex( manufacturerID ) );
    Serial.write( "\r\n" );
    Serial.write( "Device ID: 0x" );
    // expected: 2600
    Serial.write( byteToHex( deviceIDHi ) );
    Serial.write( byteToHex( deviceIDLo ) );
    // Serial.write( byteToHex( deviceID[0] ) );
    // Serial.write( byteToHex( deviceID[1] ) );
    Serial.write( "\r\n" );
}

void test_Wawe_1()
{
    while(1)
    {
        Spi.beginTransaction();
        Spi.write( 0xAF );
        Spi.endTransaction();

        for(int z=0; z<0x0000000F;) z++;
//        for(int z=0; z<0x000F0000;) z++;
    }
}

const uint32_t ADXL345_ADDRESS = 0x53;    // Assumes ALT address pin low
const uint8_t ADXL345_REG_DEVID = 0x00;    // Device ID

const uint32_t TCS34725_ADDRESS = 0x29;
const uint8_t TCS34725_REG_DEVID = 0x12;    // Device ID
const uint8_t TCS34725_COMMAND_BIT = 0x80;
const uint8_t TCS34725_CDATAL = 0x14;    /* Clear channel data */
const uint8_t TCS34725_CDATAH = 0x15;
const uint8_t TCS34725_RDATAL = 0x16;    /* Red channel data */
const uint8_t TCS34725_RDATAH = 0x17;
const uint8_t TCS34725_GDATAL = 0x18;    /* Green channel data */
const uint8_t TCS34725_GDATAH = 0x19;
const uint8_t TCS34725_BDATAL = 0x1A;    /* Blue channel data */
const uint8_t TCS34725_BDATAH = 0x1B;

const uint8_t TCS34725_INTEGRATIONTIME_700MS  = 0x00;    /**<  700ms - 256 cycles - Max Count: 65535 */
const uint8_t TCS34725_GAIN_1X                = 0x00;    /**<  No gain  */

const uint8_t TCS34725_CONTROL = 0x0F;    /* Set the gain level for the sensor */
const uint8_t TCS34725_ATIME = 0x01;    /* Integration time */

void testI2CreadColor(uint8_t color)
{
    uint8_t buf[2];
    buf[0] = 0xBE;
    buf[1] = 0xEF;

    I2c.beginTransaction( TCS34725_ADDRESS );
    I2c.write( TCS34725_COMMAND_BIT | color );
//    I2c.endTransaction();

    I2c.beginTransaction( TCS34725_ADDRESS );
    util::ByteBuffer buffer( buf, 2 );
    I2c.read( buffer );
//    I2c.endTransaction();

    Serial.write( byteToHex( buf[0] ) );
    Serial.write( byteToHex( buf[1] ) );
    Serial.write('\n');
    Serial.write('\r');
}

void testI2CInitChip()
{
    uint8_t buf[2];
    util::ByteBuffer buffer( buf, 2 );

    I2c.beginTransaction( TCS34725_ADDRESS );
    buf[0] = TCS34725_COMMAND_BIT | TCS34725_ATIME;
    buf[1] = TCS34725_INTEGRATIONTIME_700MS;
    I2c.write( buffer );
//    I2c.endTransaction();

    I2c.beginTransaction( TCS34725_ADDRESS );
    buf[0] = TCS34725_COMMAND_BIT | TCS34725_CONTROL;
    buf[1] = TCS34725_GAIN_1X;
    I2c.write( buffer );
//    I2c.endTransaction();


}

void testI2C()
{
    Serial.write( "test: I2C\n\r" );
    // uint8_t bytes[ 3 ] = { TCS34725_REG_DEVID };
    // util::ByteBuffer buffer( bytes, 3 );
    uint8_t deviceID = 0xFF;

    I2c.beginTransaction( TCS34725_ADDRESS );
    I2c.write( TCS34725_COMMAND_BIT | TCS34725_REG_DEVID );
//    I2c.endTransaction();

    I2c.beginTransaction( TCS34725_ADDRESS );
    I2c.read( &deviceID );
//    I2c.endTransaction();

    // Serial.write( "ADXL345 device ID: " );
    // should be 0xE5 ( b11100101 )
    Serial.write( "TCS34725 device ID: " );
    // should be 0x44 ( b01000100 )
    Serial.write( byteToHex( deviceID ) );
    Serial.write('\n');
    Serial.write('\r');

    testI2CInitChip();

    testI2CreadColor(TCS34725_CDATAL);
    testI2CreadColor(TCS34725_RDATAL);
    testI2CreadColor(TCS34725_GDATAL);
    testI2CreadColor(TCS34725_BDATAL);
}

gpio::Pin LED0 = pin_14;
extern char strBuf[];

void testFtoa()
{
    Serial.write(byteToHex(42));
    Serial.write("\r\n");

    util::ftoa( 3.3, 3, strBuf, 10 );
    // should print 3.299
    Serial.write( strBuf );
    Serial.write( "\r\n" );
}

void setup()
{
    LED0.direction( PinDir::Output );

    LED.on();
    for(int z=0; z<0x00080000;) z++;
    LED.off();
    // soc::powerDown();

    Serial.write('O');
    Serial.write('.');
    Serial.write('M');
    Serial.write('.');
    Serial.write('G');
    Serial.write('.');
    Serial.write('\n');
    Serial.write('\r');

    Serial.write(VERSION);
    Serial.write("\r\n");

//    testFtoa();

    // testAT45DB161D();
//    test_Wawe_1();
    testI2C();
}

bool showVoltage = true;
char strBuf[10];

void loop()
{
    if( Button.pressed() )
    {
        // LED output board lights a LED on LOW input
        LED0.low();

        if( showVoltage )
        {
            uint16_t adcData =  AD0.readRaw();

            util::toHex( adcData, strBuf, 10 );
            Serial.write( "0x" );
            Serial.write( strBuf );
            Serial.write( "\r\n" );

            float value = AD0.value();
            adcData = (uint16_t)(value * 1000);
            util::itoa( adcData, 10, strBuf, 10 );
            Serial.write( strBuf );
            Serial.write( " mV\r\n" );

            util::ftoa( value, 3, strBuf, 10 );
            Serial.write( strBuf );
            Serial.write( " V\r\n" );

            showVoltage = false;
        }
    }
    else
    {
        LED0.high();
        showVoltage = true;
    }
    // SystickTimer code (hello world blinky) will turn the LED off
    // once we release the button

    char ch;
    if( ' ' == (ch = Serial.readChar()) )
    {
        Serial.write(ch);
    }
    if( 'c' == (ch = Serial.readChar()) )
    {
        testI2CreadColor(TCS34725_CDATAL);
        testI2CreadColor(TCS34725_RDATAL);
        testI2CreadColor(TCS34725_GDATAL);
        testI2CreadColor(TCS34725_BDATAL);
        Serial.write('\n');
        Serial.write('\r');
    }

    system::yield();
}
