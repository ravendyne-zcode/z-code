// zcode
#include <zcore>
#include <boards/lpcxpresso>

// lpc_open
#include "chip.h"

namespace board
{

    Button::Button( int buttonNumber, int halPinNumber ) : gpio::hal::Pin::Pin( halPinNumber )
    {
        this->buttonNumber = buttonNumber;
        this->direction( PinDir::Input );
    }

    bool Button::pressed()
    {
        // HIGH -> pressed
        // LOW -> not pressed
        return read();
    }

    void Button::onChange( ButtonChangeCallback callback )
    {
    }

}

// ----------------------------------------------------------------------------
// There are 4 buttons on this board
// <button number>, <hal pin number>
board::Button Button1( 1, 6 );
board::Button Button2( 2, 16 );
board::Button Button3( 3, 8 );
board::Button Button4( 4, 9 );


// ----------------------------------------------------------------------------
//   STANDARD SOC DEVICE: BUTTON
// ----------------------------------------------------------------------------

board::Button Button = Button1;

