// zcode
#include <zcore>
#include <boards/lpcxpresso>

// lpc_open
#include "chip.h"

namespace board
{

    Led::Led( int halPinNumber ) : gpio::hal::Pin::Pin( halPinNumber )
    {
        this->direction( PinDir::Output );
    }

    void Led::on()
    {
        this->high();
    }

    void Led::off()
    {
        this->low();
    }

}

// ----------------------------------------------------------------------------
//   STANDARD SOC DEVICE: LED
// ----------------------------------------------------------------------------

board::Led LED( 7 );
