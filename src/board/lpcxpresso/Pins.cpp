// zcode
#include <zcore>
#include <boards/lpcxpresso>

// lpc_open
#include "chip.h"

namespace gpio
{
    namespace hal
    {

        const PortPin HalPinMap[] =
        {
            //--------------------------
            // DIGITAL pins
            //
            // - 0
            { 1, 19 },
            { 0, 17 },
            { 0, 20 },
            { 1, 13 },
            // - 4
            { 1, 14 },
            { 1, 15 },
            { 1, 16 },
            { 0, 7 }, // LED -> pin 7
            // - 8
            { 1, 28 },
            { 1, 27 },
            { 1, 26 },
            { 1, 25 },
            // - 12
            { 1, 24 },
            { 0, 22 },
            { 0, 21 },
            { 0, 1 },
            // - 16
            { 1, 31 },

            //--------------------------
            // ANALOG pins
            //
            { 0,  11, },
            { 0,  12, },
            { 0,  13, },
            // - 20
            { 0,  14, },
            { 0,  16, },
            { 0,  23, },

            //--------------------------
            // DESIGNATED pins
            //
            // I2C
            { 0,  4,  },
            // - 24
            { 0,  5,  },

            // USART0
            { 0,  18, },
            { 0,  19, },

            // SPI1
            { 1,  23, },
            // - 28
            { 1,  20, },
            { 1,  21, },
            { 1,  22, },

            // SPECIAL FUNCTION
            { 0,  3,  },
            // - 32
            { 0,  6,  },

            // SPI0 - can't use this on lpcxpresso, JTAG is connected to PIO0_9
            /*
            { 0,  2,  },
            { 1,  29, },
            { 0,  8,  },
            { 0,  9,  },
            */
        };

        const uint32_t halPinCount = sizeof( HalPinMap ) / sizeof( PortPin );
    }
}

const uint16_t PinModeGPIO[] = {
   // DIGITAL
    /*1,  19,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*0,  17,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*0,  20,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*1,  13,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),

    /*1,  14,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*1,  15,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*1,  16,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    // LED
    /*0,  7,*/   ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ),

    /*1,  28,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*1,  27,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*1,  26,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*1,  25,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),

    /*1,  24,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*0,  22,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    /*0,  21,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    // This is also used as ISP entry pin. When LPC1347 comes out of reset, if this pin is LOW, ISP mode is entered
    /*0,  1,*/   ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),

    /*1,  31,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),


    // ADC0-5
    /*0,  11,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*0,  12,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*0,  13,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*0,  14,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*0,  16,*/ ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*0,  23,*/ ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),




    // I2C
    /*0,  4,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_STDI2C_EN ),
    /*0,  5,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_STDI2C_EN ),

    // USART0
    /*0,  18,*/ ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*0,  19,*/ ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),

    // SPI1
    /*1,  23,*/ ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*1,  20,*/ ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*1,  21,*/ ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),
    /*1,  22,*/ ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ),

    // SPECIAL FUNCTION
    /*0,  3,*/  ( PIN_FUNCTION_RESERVED ),
    /*0,  6,*/  ( PIN_FUNCTION_RESERVED ),

    // SPI0 - can't use this on lpcxpresso, JTAG is connected to PIO0_9
    // /*0,  2,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    // /*1,  29,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    // /*0,  8,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
    // /*0,  9,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ),
};

const uint16_t PinModeADC[] = {
   // DIGITAL
    /*1,  19,*/  ( PIN_FUNCTION_NONE ),
    /*0,  17,*/  ( PIN_FUNCTION_NONE ),
    /*0,  20,*/  ( PIN_FUNCTION_NONE ),
    /*1,  13,*/  ( PIN_FUNCTION_NONE ),

    /*1,  14,*/  ( PIN_FUNCTION_NONE ),
    /*1,  15,*/  ( PIN_FUNCTION_NONE ),
    /*1,  16,*/  ( PIN_FUNCTION_NONE ),
    // LED
    /*0,  7,*/   ( PIN_FUNCTION_NONE ),

    /*1,  28,*/  ( PIN_FUNCTION_NONE ),
    /*1,  27,*/  ( PIN_FUNCTION_NONE ),
    /*1,  26,*/  ( PIN_FUNCTION_NONE ),
    /*1,  25,*/  ( PIN_FUNCTION_NONE ),

    /*1,  24,*/  ( PIN_FUNCTION_NONE ),
    /*0,  22,*/  ( IOCON_FUNC1 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ),
    /*0,  21,*/  ( PIN_FUNCTION_NONE ),
    // This is also used as ISP entry pin. When LPC1347 comes out of reset, if this pin is LOW, ISP mode is entered
    /*0,  1,*/   ( PIN_FUNCTION_NONE ),

    /*1,  31,*/  ( PIN_FUNCTION_NONE ),


    // ADC0-5
    /*0,  11,*/ ( IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ),
    /*0,  12,*/ ( IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ),
    /*0,  13,*/ ( IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ),
    /*0,  14,*/ ( IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ),
    /*0,  16,*/ ( IOCON_FUNC1 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ),
    /*0,  23,*/ ( IOCON_FUNC1 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ),




    // I2C
    /*0,  4,*/  ( PIN_FUNCTION_NONE ),
    /*0,  5,*/  ( PIN_FUNCTION_NONE ),

    // USART0
    /*0,  18,*/ ( PIN_FUNCTION_NONE ),
    /*0,  19,*/ ( PIN_FUNCTION_NONE ),

    // SPI1
    /*1,  23,*/ ( PIN_FUNCTION_NONE ),
    /*1,  20,*/ ( PIN_FUNCTION_NONE ),
    /*1,  21,*/ ( PIN_FUNCTION_NONE ),
    /*1,  22,*/ ( PIN_FUNCTION_NONE ),

    // SPECIAL FUNCTION
    /*0,  3,*/  ( PIN_FUNCTION_RESERVED ),
    /*0,  6,*/  ( PIN_FUNCTION_RESERVED ),

    // SPI0 - can't use this on lpcxpresso, JTAG is connected to PIO0_9
    // /*0,  2,*/  ( PIN_FUNCTION_NONE ),
    // /*1,  29,*/ ( PIN_FUNCTION_NONE ),
    // /*0,  8,*/  ( PIN_FUNCTION_NONE ),
    // /*0,  9,*/  ( PIN_FUNCTION_NONE ),
};

const uint16_t PinModePWM[] = {
   // DIGITAL
    /*1,  19,*/  ( PIN_FUNCTION_NONE ),
    /*0,  17,*/  ( PIN_FUNCTION_NONE ),
    /*0,  20,*/  ( PIN_FUNCTION_NONE ),
    /*1,  13,*/  ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT16B0_MAT0

    /*1,  14,*/  ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT16B0_MAT1
    /*1,  15,*/  ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT16B0_MAT2
    /*1,  16,*/  ( PIN_FUNCTION_NONE ),
    // LED
    /*0,  7,*/   ( PIN_FUNCTION_NONE ),

    /*1,  28,*/  ( PIN_FUNCTION_NONE ),
    /*1,  27,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B0_MAT3
    /*1,  26,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B0_MAT2
    /*1,  25,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B0_MAT1

    /*1,  24,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B0_MAT0
    /*0,  22,*/  ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT16B1_MAT1
    /*0,  21,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT16B1_MAT0
    // This is also used as ISP entry pin. When LPC1347 comes out of reset, if this pin is LOW, ISP mode is entered
    /*0,  1,*/   ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B0_MAT2

    /*1,  31,*/  ( PIN_FUNCTION_NONE ),


    // ADC0-5
    /*0,  11,*/ ( IOCON_FUNC3 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B0_MAT3
    /*0,  12,*/ ( PIN_FUNCTION_NONE ),
    /*0,  13,*/ ( IOCON_FUNC3 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B1_MAT0
    /*0,  14,*/ ( IOCON_FUNC3 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B1_MAT1
    /*0,  16,*/ ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B1_MAT3
    /*0,  23,*/ ( PIN_FUNCTION_NONE ),




    // I2C
    /*0,  4,*/  ( PIN_FUNCTION_NONE ),
    /*0,  5,*/  ( PIN_FUNCTION_NONE ),

    // USART0
    /*0,  18,*/ ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B0_MAT0
    /*0,  19,*/ ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT32B0_MAT1

    // SPI1
    /*1,  23,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT16B1_MAT1
    /*1,  20,*/ ( PIN_FUNCTION_NONE ),
    /*1,  21,*/ ( PIN_FUNCTION_NONE ),
    /*1,  22,*/ ( PIN_FUNCTION_NONE ),

    // SPECIAL FUNCTION
    /*0,  3,*/  ( PIN_FUNCTION_RESERVED ),
    /*0,  6,*/  ( PIN_FUNCTION_RESERVED ),

    // SPI0 - can't use this on lpcxpresso, JTAG is connected to PIO0_9
    // /*0,  2,*/  ( PIN_FUNCTION_NONE ),
    // /*1,  29,*/ ( PIN_FUNCTION_NONE ),
    // /*0,  8,*/  ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT16B0_MAT0
    // /*0,  9,*/  ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ), // CT16B0_MAT1
};



const SocPinMode InitialSocPinMode[] = {
    /*
        LPC1347 doesn't have pins:
        0.24 - 0.31, 1.6, 1.12 and 1.30
    */

    /*
    // SWD
    { 0,  0, reset },
    { 0,  9, SWO },
    { 0,  10, SWCLK },
    { 0,  15, SWDIO },
    */

    /*
    // MISSING on LQFP48 package
    { 1,  0, _ },
    { 1,  1, _ },
    { 1,  2, _ },
    { 1,  3, _ },
    { 1,  4, _ },
    { 1,  5, _ },
    { 1,  7, _ },
    { 1,  8, _ },
    { 1,  10, _ },
    { 1,  11, _ },
    { 1,  17, RXD },
    { 1,  18, TXD },
    */

    // DIGITAL
    { /*1,  19,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_19 -> digital 0
    { /*0,  17,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO0_17 -> digital 1
    { /*0,  20,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO0_20 -> digital 2
    { /*1,  13,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_13 -> digital 3

    { /*1,  14,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_14 -> digital 4
    { /*1,  15,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_15 -> digital 5
    { /*1,  16,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_16 -> digital 6
    // LED
    { /*0,  7,*/   ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ) },   // PIO0_7 -> digital 7, on-board LED

    { /*1,  28,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_28 -> digital 8
    { /*1,  27,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_27 -> digital 9
    { /*1,  26,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_26 -> digital 10
    { /*1,  25,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_25 -> digital 11

    { /*1,  24,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_24 -> digital 12
    { /*0,  22,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO0_22 -> digital 13
    { /*0,  21,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO0_21 -> digital 14
    // This is also used as ISP entry pin. When LPC1347 comes out of reset, if this pin is LOW, ISP mode is entered
    { /*0,  1,*/   ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO0_1 -> digital 15

    { /*1,  31,*/  ( IOCON_FUNC0 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLUP ) },      // PIO1_31 -> digital 16


    // ADC0-5
    { /*0,  11,*/ ( IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ) },          // PIO0_11 -> ADC0
    { /*0,  12,*/ ( IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ) },          // PIO0_12 -> ADC1
    { /*0,  13,*/ ( IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ) },          // PIO0_13 -> ADC2
    { /*0,  14,*/ ( IOCON_FUNC2 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ) },          // PIO0_14 -> ADC3
    { /*0,  16,*/ ( IOCON_FUNC1 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ) },          // PIO0_16 -> ADC4
    { /*0,  23,*/ ( IOCON_FUNC1 | IOCON_ADMODE_EN    | IOCON_FILT_DIS ) },          // PIO0_23 -> ADC5




    // I2C
    { /*0,  4,*/  ( IOCON_FUNC1 | IOCON_STDI2C_EN ) },                               // PIO0_4 -> SCL
    { /*0,  5,*/  ( IOCON_FUNC1 | IOCON_STDI2C_EN ) },                               // PIO0_5 -> SDA

    // USART0
    { /*0,  18,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },      // PIO0_18 -> RXD
    { /*0,  19,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },      // PIO0_19 -> TXD

    // SPI1
    { /*1,  23,*/ ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },      // PIO1_23 -> SSEL1
    { /*1,  20,*/ ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },      // PIO1_20 -> SCK1
    { /*1,  21,*/ ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },      // PIO1_21 -> MISO1
    { /*1,  22,*/ ( IOCON_FUNC2 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ) },   // PIO1_22 -> MOSI1 (mode PULLDOWN keeps MOSI LOW when idle)

    // SPECIAL FUNCTION
    { /*0,  3,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },      // PIO0_3 -> USB_VBUS
    { /*0,  6,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },      // PIO0_6 -> USB_CONNECT

    // SPI0 - can't use this on lpcxpresso, JTAG is connected to PIO0_9
    // https://community.nxp.com/thread/389121
    // https://community.nxp.com/thread/389084
    // http://www.nxp.com/docs/en/user-guide/LPCXpresso_IDE_SWO_Trace.pdf
    // { /*0,  2,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },     // PIO0_2 -> SSEL0
    // { /*1,  29,*/ ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },     // PIO1_29 -> SCK0
    // { /*0,  8,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_INACT ) },     // PIO0_8 -> MISO0
    // { /*0,  9,*/  ( IOCON_FUNC1 | IOCON_RESERVED_BIT_7 | IOCON_MODE_PULLDOWN ) },     // PIO0_9 -> MOSI0
};


const uint32_t InitialSocPinModeLength = sizeof( InitialSocPinMode ) / sizeof( SocPinMode );


// ----------------------------------------------------------------------------
//   STANDARD SOC DEVICE: PINS
// ----------------------------------------------------------------------------

gpio::Pin pin_0( 0 );
// gpio::Pin pin_1( 1 ); // SSEL
gpio::Pin pin_2( 2 );
gpio::Pin pin_3( 3 );
gpio::Pin pin_4( 4 );
gpio::Pin pin_5( 5 );
// gpio::Pin pin_6( 6 ); // Button1
// Taken by on-board LED
//gpio::Pin pin_7( 7 );
// gpio::Pin pin_8( 8 ); // Button2
// gpio::Pin pin_9( 9 ); // Button3
gpio::Pin pin_10( 10 );
gpio::Pin pin_11( 11 );
gpio::Pin pin_12( 12 );
gpio::Pin pin_13( 13 );
gpio::Pin pin_14( 14 );
gpio::Pin pin_15( 15 );
// gpio::Pin pin_15( 16 ); // Button2
