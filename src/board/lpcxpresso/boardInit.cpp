// zcode
#include <zcore>
#include <boards/lpcxpresso>

// lpc_open
#include "chip.h"

#ifdef __cplusplus
extern "C" {
#endif

/* System oscillator rate and clock rate on the CLKIN pin */
/*
  Needed by Chip_Clock_GetMainOscRate() in clock_13xx.h
  and ultimately used by Chip_Clock_GetMainClockRate() (and others)
  which is then called by other functions like SPI, UART, I2C etc.
*/
const uint32_t OscRateIn = 12000000;
/* Not used but needs to be declared */
const uint32_t ExtRateIn = 0;


#ifdef __cplusplus
}
#endif



void board::setupClocking()
{
    Chip_SetupXtalClocking();
    SystemCoreClockUpdate();
}

void board::initialize()
{
}

void board::setupPins()
{
    for (uint32_t idx = 0; idx < gpio::hal::halPinCount; idx++ )
    {
        // get port/pin from HAL->SoC mapping array and modefunc from InitialSocPinMode array
        Chip_IOCON_PinMuxSet( LPC_IOCON, gpio::hal::HalPinMap[ idx ].port, gpio::hal::HalPinMap[ idx ].pin, InitialSocPinMode[ idx ].modefunc );
    }
}
