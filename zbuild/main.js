'use strict'

const _ = require( 'lodash' )
const fs = require( 'fs' )
const nopt = require( 'nopt' )

import * as zutil from './lib/util'
import Config from './lib/config'


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// INTERNAL FUNCTIONS
//

const mergeBuildConfigOptions = function( options, config )
{
    let build_options = _.merge( {}, options )

    let opt = {}
    _.merge( opt, config.get( 'zbuild.options' ) )

    for( let prop in opt )
    {
        build_options[ prop ] = opt[ prop ]
    }

    return build_options
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// PARSE COMMAND LINE ARGUMENTS
//

const knownOpts =
{
// zbuild
    'debug': Boolean,
    'verbose': Boolean,
    'conf': String,

// cmd: compile, link
    'app': Boolean,
    'zcode': Boolean,
    'vendor': Boolean,

// opt: clean
    'full': Boolean,

// opt: vendor
    'vname':
    [
        'lpc_open',
    ],

    'variant':
    [
        'lpc_chip_13xx',
    ],

// opt: zcode, app
    'cpu':
    [
        'cortex-m3',
    ],
    'soc':
    [
        'lpc1347',
        'qemu',
    ],
    'board':
    [
        'lpcxpresso',
    ],

// opt: run
    'name': String,

// other
    'version': Boolean,
}

const shortHands =
{
}

var parsed = nopt( knownOpts, shortHands, process.argv, 2 )

if( parsed.version )
{
    console.log( 'zbuild v' + zbuild_version )
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// COMMAND
//

// default command is 'build'
var command = parsed.argv.remain.length ? parsed.argv.remain[ 0 ] : 'build'



//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// CONFIG BASE
//

var config = new Config( zutil.require( 'config/project.config' ) )

if( ! config.get('options.CONFIG_BASE') )
{
    config.set( 'options.CONFIG_BASE', process.cwd() )
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// BUILD OPTIONS
//

var configfile = 'config/build.config'
if( parsed.conf )
{
    configfile = parsed.conf
}

var build_config = new Config()
if( fs.existsSync( configfile ) )
{
    build_config = new Config( zutil.require( configfile ) )

    parsed = mergeBuildConfigOptions( parsed, build_config )

    // make build options available for template expansion in other config files
    config.mergeA( build_config )
}
else
{
    // if there is no config file, get the cli options merged into main config
    let data =
    {
        data:
        {
            cpu: parsed.cpu,
            soc: parsed.soc,
            board: parsed.board,
            vendor: parsed.vname,
            variant: parsed.variant,
        }
    }

    let build_config_tempate =
    {
        zbuild:
        {
            options:
            {
                cpu: '<%= data.cpu %>',
                soc: '<%= data.soc %>',
                board: '<%= data.board %>',
                vendor: '<%= data.vendor %>',
                variant: '<%= data.variant %>',
            },
        },
    }

    let options_config = new Config( build_config_tempate )
    options_config.merge( data )
    options_config.finalize()

    config.mergeA( options_config )
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// DO BUILD
//

import zbuild from './lib/zbuild.js'

zbuild( command, config, parsed )
