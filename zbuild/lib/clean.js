'use strict'

const path = require( 'path' )
const shell = require( 'shelljs' )
shell.config.silent = true

//////////////////////////////////////////////////////////////////////////////////////////////////////

var log = null

//////////////////////////////////////////////////////////////////////////////////////////////////////

const commandClean = function( config, options )
{
    if( options.full )
    {
        log.verbose( 'full clean' )

        let foldersToRemove = shell.ls('-d', 'build/*').map( function( el ) { return el } )

        log.verbose( 'removing folders: ', foldersToRemove )
        log.debug( 'foldersToRemove:', foldersToRemove )

        if( foldersToRemove.length == 0 )
        {
            log.warn( 'no files to clean' )
            return Promise.resolve()
        }

        let result = shell.rm( '-rf', foldersToRemove )
        if( result.code != 0 )
        {
            log.error()
        }
        else
        {
            log.ok()
        }
        return Promise.resolve()
    }

    let extensionsToClean =
    [
        '.o',
        '.a',

        '.axf',
        '.elf',
        '.bin',

        '.lst',
        '.lss',
        '.map',
    ]

    let filesToClean = shell.find('build').filter( function( file )
    {
        let ext = path.extname( file )
        if( extensionsToClean.includes( ext ) )
        {
            return true
        }

        return false
    })

    log.debug( filesToClean )

    if( filesToClean.length == 0 )
    {
        log.warn( 'no files to clean' )
        return Promise.resolve()
    }

    let result = shell.rm( filesToClean )
    if( result.code != 0 )
    {
        log.error()
    }
    else
    {
        log.ok()
    }

    return Promise.resolve()
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

export default function( zbuild )
{
    log = zbuild.log
    return commandClean
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
