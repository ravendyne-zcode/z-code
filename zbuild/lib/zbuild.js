'use strict'

const _ = require( 'lodash' )

import Log from './log.js'


import cmd_compile from './compile.js'
import cmd_link from './link.js'
import cmd_clean from './clean.js'
import cmd_run from './run.js'



//////////////////////////////////////////////////////////////////////////////////////////////////////

var log = null
var zbuildCommands = {}

//////////////////////////////////////////////////////////////////////////////////////////////////////

const createLogObject = function( options )
{
    let logOptions = {}
    if( options.debug )
    {
        logOptions.debug = true
    }
    if( options.verbose )
    {
        logOptions.verbose = true
    }

    return new Log( logOptions )
}



const executeBuildCommand = function( command, config, options )
{
    log.debug( 'command:', command )
    if( zbuildCommands[ command ] )
    {
        log.debug( 'executing ...' )
        zbuildCommands[ command ]( config, options )
        // To avoid 'Unhandled promise rejection' deprecation warning
        // we need to 'handle' last rejected promise in the chain
        // (which we don't care about here).
        // Because better.
        .then( function(){}, function(){} )
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////


export default function( command, config, options )
{

//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// CREATE LOG OBJECT
//

log = createLogObject( options )


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// OPERATIONS
//

const zbuild =
{
    log: log,
}

zbuildCommands =
{
    clean: cmd_clean( zbuild ),
    compile: cmd_compile( zbuild ),
    link: cmd_link( zbuild ),
    build: function( config, options )
    {
        return zbuildCommands.compile( config, options )
        .then( function()
        {
            return zbuildCommands.link( config, options )
        },
        // don't care about failed promise here
        function(){})
    },
    run: cmd_run( zbuild ),
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// EXECUTE
//

executeBuildCommand( command, config, options )


//////////////////////////////////////////////////////////////////////////////////////////////////////
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
