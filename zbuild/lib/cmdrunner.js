'use strict'

const _ = require( 'lodash' )

const spawn = require('child_process').spawn
const which = require('which').sync
const asyncSeries = require('async/series')

//////////////////////////////////////////////////////////////////////////////////////////////////////

var log = null

//////////////////////////////////////////////////////////////////////////////////////////////////////

// Spawn a child process, capturing its stdout and stderr.
const util_spawn = function( opts, doneCallback )
{
    // Build a result object and pass it (among other things) into the
    // doneCallback function.
    var callDoneCallback = function( code, stdout, stderr )
    {
        // Remove trailing whitespace (newline)
        stdout = _.trimEnd(stdout)
        stderr = _.trimEnd(stderr)

        // Create the result object.
        var result =
        {
            stdout: stdout,
            stderr: stderr,
            code: code,
            toString: function()
            {
                if( code === 0 )
                {
                    return stdout
                }

                return stderr
            }
        }

        // On error (and no fallback) pass an error object, otherwise pass null.
        doneCallback( code === 0 || new Error( stderr ), result, code )
    }

    var cmd, args
    var pathSeparatorRe = /[\\\/]/g

    // On Windows, child_process.spawn will only file .exe files in the PATH,
    // not other executable types (grunt issue #155).
    try
    {
        if( ! pathSeparatorRe.test( opts.cmd ) )
        {
            // Only use which if cmd has no path component.
            cmd = which( opts.cmd )
        }
        else
        {
            cmd = opts.cmd.replace( pathSeparatorRe, path.sep )
        }
    }
    catch( err )
    {
        callDoneCallback( 127, '', String( err ) )
        return
    }

    args = opts.args || []

    var child = spawn( cmd, args, opts.opts )
    var stdout = new Buffer('')
    var stderr = new Buffer('')

    if( child.stdout )
    {
        child.stdout.on( 'data', function( buf )
        {
            stdout = Buffer.concat( [ stdout, new Buffer( buf ) ] )
        })
    }

    if( child.stderr )
    {
        child.stderr.on( 'data', function( buf )
        {
            stderr = Buffer.concat( [ stderr, new Buffer( buf ) ] )
        })
    }

    child.on( 'close', function( code )
    {
        callDoneCallback( code, stdout.toString(), stderr.toString() )
    })

    return child
}


//////////////////////////////////////////////////////////////////////////////////////////////////////


const commandRunner = function( command, done )
{
    if( ! command )
    {
        return false
    }

    var cmd = command.cmd
    var args = command.args
    var echo = command.echo || ( 'running cmd > ' + cmd )

    var alwaysShowCmdOutput = command.alwaysShowCmdOutput || false
    var ignoreCmdErrors = command.ignoreCmdErrors || false

    log.debug( 'cmd', cmd )
    log.debug( 'args', args )
    log.debug( 'echo', echo )
    log.debug( 'alwaysShowCmdOutput', alwaysShowCmdOutput )
    log.debug( 'ignoreCmdErrors', ignoreCmdErrors )

    var alignTo = 50
    echo = _.truncate( echo, { length: alignTo } )
    echo = _.padEnd( echo, alignTo )
    log.write( echo )

    if( ! cmd || ! args )
    {
        log.writeln( ( 'need cmd and args' ).yellow )

        return
    }


    log.debug( 'EXEC >> ' + command.cmd + ' ' + command.args.join(' ') )

    util_spawn({
        cmd: cmd,
        args: args,
    }, function ( error, result, code )
    {
        if( code === 0 )
        {
            log.ok()
            if( alwaysShowCmdOutput )
            {
                if( result.stdout != '' )
                    log.writeln( result.stdout )
                if( result.stderr != '' )
                    log.writeln( result.stderr )
            }
            done( true )
        }
        else
        {
            log.error()
            if( result.stdout != '' )
                log.writeln( result.stdout )
            if( result.stderr != '' )
                log.writeln( result.stderr )
            done( ignoreCmdErrors )
        }
    })
}

//////////////////////////////////////////////////////////////////////////////////////////////////////


const executeCommands = function( commandArray )
{
    let resolve_promise = null
    let reject_promise = null

    let promise = new Promise( function( resolve, reject )
    {
        resolve_promise = resolve
        reject_promise = reject
    })

    let asyncTaskArray = commandArray.map( function( el )
    {
        return function( cb )
        {
            const done = function( val )
            {
                if( val )
                {
                    cb( null, true )
                }
                else
                {
                    cb( false, null )
                }
            }

            commandRunner( el, done )
        }
    })

    asyncSeries( asyncTaskArray, function( err, results )
    {
        if( err )
        {
            log.error()
            log.fail( err )
            // reject_promise( err )
        }
        else
        {
            resolve_promise( results )
        }
    })

    return promise
}


//////////////////////////////////////////////////////////////////////////////////////////////////////


export default function( zbuild )
{
    log = zbuild.log
    return executeCommands
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
