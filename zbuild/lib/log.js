'use strict'

const util = require( 'util' )
const colors = require( 'colors' )

function Log( options )
{
    this.options = Object.assign( {},
    {
        color: true,
        verbose: false,
        debug: false,
        outStream: process.stdout,
    },
    options )
}

Object.assign( Log.prototype,
{
    ok: function()
    {
        this.writeln( 'OK'.green )
        return this
    },

    error: function()
    {
        this.writeln( 'ERROR'.red )
        return this
    },

    success: function( msg )
    {
        this.writeln( msg.green )
        return this
    },

    fail: function( msg )
    {
        this.writeln( msg.red )
        return this
    },

    warn: function( msg )
    {
        this.writeln( msg.yellow )
        return this
    },

    info: function( msg )
    {
        this.writeln( msg.cyan )
        return this
    },

    verbose: function()
    {
        if( ! this.options.verbose )
        {
            return
        }

        this.write.apply( this, arguments )
        this.writeln()

        return this
    },

    debug: function()
    {
        if( ! this.options.debug )
        {
            return
        }

        this.writeln()
        this.write( '[D]'.magenta )
        this.write.apply( this, arguments )
        this.writeln()

        return this
    },

    _write: function( msg )
    {
        this.options.outStream.write( msg )
        return this
    },

    write: function()
    {
        var self = this
        var notHead = false
        var args = Array.prototype.slice.call( arguments )

        args.forEach( function( arg )
        {
            if( notHead )
            {
                self._write( ' ' )
            }
            notHead = true

            if( typeof arg === 'string' )
            {
                self._write( arg )
            }
            else
            {
                self._write( util.inspect( arg ) )
            }
        } )

        return this
    },

    writeln: function()
    {
        this.write.apply( this, arguments )
        this.write( '\n' )
        return this
    },

} )

export default Log
