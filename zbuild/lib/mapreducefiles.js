'use strict'

const _ = require( 'lodash' )
const path = require( 'path' )
const fs = require( 'fs' )
const shell = require( 'shelljs' )


import * as zutil from './util.js'
import Config from './config.js'
import Log from './log.js'

const objExt = '.o'
const configBaseRootForMap = 'compile'
const configBaseRootForReduce = 'link'
const sourcesOptions =
[
    'cppsource',
    'csource',
    'assembler',
]

//////////////////////////////////////////////////////////////////////////////////////////////////////

var log = null

//////////////////////////////////////////////////////////////////////////////////////////////////////

const expandUsingLodash = function( args, values )
{
    // Set {%%} as template delimiters
    let templateOptions =
    {
        'escape': /{%-([\s\S]+?)%}/g,
        'evaluate': /{%([\s\S]+?)%}/g,
        'interpolate': /{%=([\s\S]+?)%}/g,
    }

    // Expand {%%} templates in args
    let expandedArgs = args.map(
        function ( val )
        {
            let compiled = _.template( val, templateOptions )
            return compiled( values )
        })

    return expandedArgs
}

const expandUsingConfig = function( args, values )
{
    // we'll use Config class to process arguments
    // since it won't convert arrays to strings on replacement
    // like _.template() does
    let argz = args.map( function ( val )
        {
            val = val.replace('{%=', '<%=')
            val = val.replace('%}', '%>')
            return val
        })

    let myconfig = new Config( values )
    myconfig.merge( { args: argz } )
    let expandedArgs = _.compact( _.flatten( myconfig.get( 'args' ) ) )

    return expandedArgs
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

const collectSourceFilesForMap = function( config, configBranch, module )
{
    let configBase = configBaseRootForMap + configBranch

    //
    // requires
    //
    config.requires( configBaseRootForMap + module + '.options.src' )
    config.requires( configBaseRootForMap + module + '.options.dest' )
    config.requires( 'sources' + configBranch + '.files' )

    //
    // get config values
    //
    var src = config.get( configBaseRootForMap + module + '.options.src' )
    var dest = config.get( configBaseRootForMap + module + '.options.dest' )

    var fileList = config.get( 'sources' + configBranch + '.files' )


    fileList = fileList.map( function( file )
    {
        let srcFile = path.join( src, file )
        let destFile = path.join( dest, file )

        let srcPathParts = path.parse( srcFile )
        let destPathParts = path.parse( destFile )

        let name = srcPathParts.name

        let destPath = destPathParts.dir
        destFile = path.join( destPath, destPathParts.name ) + objExt

        return {
            destPath: destPath,
            name: name,
            srcFile: srcFile,
            destFile: destFile,
            file: file,
        }
    })

    return fileList
}

const _processFilesForMap = function( config, configBranch, module )
{
    let configBase = configBaseRootForMap + configBranch

    var fileList = collectSourceFilesForMap( config, configBranch, module )
    if( fileList.length == 0 )
    {
        return []
    }

    //
    // requires
    //
    config.requires( configBase + '.cmd' )
    config.requires( configBase + '.args' )

    //
    // get config values
    //
    let alwaysShowCmdOutput = config.get( configBase + '.alwaysShowCmdOutput' ) || false
    var cmd = config.get( configBase + '.cmd' )
    var args = config.get( configBase + '.args' )
    var echo = config.get( configBase + '.echo' ) || 'MAP'


    var baseFolder = zutil.baseFolder
    var args = _.compact( _.flatten( args ) )

    let commandList = fileList.map( function( fileData )
    {
        //
        // make sure all folders along the path to
        // the final destination exist
        //
        if( ! fs.existsSync( fileData.destPath ) )
        {
            log.debug( 'creating folder: ' + fileData.destPath )
            shell.mkdir( '-p', fileData.destPath )
        }

        //
        // process args by expanding any '{%%}' template values
        //
        let options =
        {
            // Data to be used to expand {%%} templates
            name: fileData.name,
            basename: path.join( baseFolder, fileData.destPath, fileData.name ),
            inputFile: path.join( baseFolder, fileData.srcFile ),
            outputFile: path.join( baseFolder, fileData.destFile ),
        }

        let expandedArgs = expandUsingConfig( args, options )

        return {
            cmd: cmd,
            args: expandedArgs,
            echo: echo + ' ' + fileData.file,
            alwaysShowCmdOutput: alwaysShowCmdOutput,
            options: options,
        }
    } )

    //
    // done
    //
    return commandList
}

const processFilesForMap = function( config, module )
{
    let commands = []

    sourcesOptions.forEach( function( sourceOption )
    {
        commands = commands.concat( _processFilesForMap( config, '.' + sourceOption, module ) )
    } )

    return commands
}


//////////////////////////////////////////////////////////////////////////////////////////////////////

const collectSourceFilesForReduce = function( config, configBranch )
{
    var baseFolder = zutil.baseFolder
    let configBase = configBaseRootForReduce + configBranch

    //
    // requires
    //
    config.requires( configBase + '.src' )
    sourcesOptions.forEach( function( sourceOption )
    {
        config.requires( 'sources.' + sourceOption + '.files' )
    } )

    //
    // get config values
    //
    var src = config.get( configBase + '.src' )

    //
    // collect sources
    //
    let fileList = []
    sourcesOptions.forEach( function( sourceOption )
    {
        fileList = fileList.concat( config.get( 'sources.' + sourceOption + '.files' ) )
    } )

    // replace extensions of all files with objExt
    fileList = fileList.map( function( file )
    {
        let parts = path.parse( file )
        let srcFile = path.join( parts.dir, parts.name ) + objExt
        return path.join( baseFolder, src, srcFile )
    })

    return fileList
}

const processFilesForReduce = function( config, configBranch )
{
    let configBase = configBaseRootForReduce + configBranch

    var fileList = collectSourceFilesForReduce( config, configBranch )
    if( fileList.length == 0 )
    {
        return []
    }

    //
    // requires
    //
    config.requires( configBase + '.dest' )
    config.requires( configBase + '.target' )
    config.requires( configBase + '.cmd' )
    config.requires( configBase + '.args' )

    //
    // get config values
    //
    let alwaysShowCmdOutput = config.get( configBase + '.alwaysShowCmdOutput' ) || false
    let dest = config.get( configBase + '.dest' )
    let target = config.get( configBase + '.target' )
    let cmd = config.get( configBase + '.cmd' )
    let args = config.get( configBase + '.args' )
    let echo = config.get( configBase + '.echo' ) || 'MAP'


    //
    // make sure entire path to destination folder exists
    //
    var destPath = path.join( dest, path.dirname( target ) )

    if( ! fs.existsSync( destPath ) )
    {
        log.debug( 'creating folder: ' + destPath )
        shell.mkdir( '-p', destPath )
    }

    var baseFolder = zutil.baseFolder
    var outputFile = path.join( baseFolder, dest, target )

    let options =
    {
        // Data to be used to expand {%%} templates
        basename: outputFile,
        inputFiles: fileList,
        outputFile: outputFile,
    }

    //
    // process args by flattening it into 1-dimensional array
    // and expanding any '{%%}' template values
    //
    args = _.compact( _.flatten( args ) )
    args = expandUsingConfig( args, options )

    //
    // done
    // command list is just one command
    //
    return [{
        cmd: cmd,
        args: args,
        echo: echo + ' ' + target,
        alwaysShowCmdOutput: alwaysShowCmdOutput,
        options: options,
    }]
}


//////////////////////////////////////////////////////////////////////////////////////////////////////

export default function( zbuild )
{
    log = zbuild.log

    return {
        processFilesForMap: processFilesForMap,
        processFilesForReduce: processFilesForReduce,
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////
