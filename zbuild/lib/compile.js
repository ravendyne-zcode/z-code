'use strict'

const _ = require( 'lodash' )

import * as zutil from './util.js'
import Config from'./config.js'

import cmdrunner from './cmdrunner.js'
import mapreducefiles from './mapreducefiles.js'


//////////////////////////////////////////////////////////////////////////////////////////////////////

var mr = null
var executeCommands = null
var log = null

//////////////////////////////////////////////////////////////////////////////////////////////////////

const commandCompile = function( config, options )
{
    let compile_commands = []

    if( options.zcode )
    {
        let cpu = options.cpu
        let soc = options.soc
        let board = options.board

        if( ! cpu || ! soc || ! board )
        {
            log.fail( "Please specify 'cpu', 'soc' and 'board' parameters for compilation." )
            return Promise.reject()
        }

        // compiler configuration: commands, arguments, options etc.
        config.mergeA( zutil.require( 'config/compile/' + cpu + '.config' ) )
        config.mergeA( zutil.require( 'config/compile/zcode.options.config' ) )

        // source files to compile
        config.mergeA( zutil.require( 'config/sources/compiler.config' ) )
        config.mergeA( zutil.require( 'config/sources/zcode.config' ) )
        config.mergeA( zutil.require( 'config/sources/cpu/' + cpu + '.config' ) )
        config.mergeA( zutil.require( 'config/sources/soc/' + soc + '.config' ) )
        config.mergeA( zutil.require( 'config/sources/board/' + board + '.config' ) )

        config.finalize()

        compile_commands = mr.processFilesForMap( config, '.zcode' )
    }
    else if( options.vendor )
    {
        let variant = options.variant
        let vname = options.vname

        if( ! variant )
        {
            log.fail( "Please specify 'variant' parameter for compilation." )
            return Promise.reject()
        }

        config.mergeA( zutil.require( 'config/vendor/' + vname + '/' + variant + '.compile.config' ) )
        config.mergeA( zutil.require( 'config/vendor/' + vname + '/' + variant + '.sources.config' ) )

        config.finalize()

        compile_commands = mr.processFilesForMap( config, '.' + variant )
    }
    else if( options.app )
    {
        let cpu = options.cpu
        let soc = options.soc
        let board = options.board

        if( ! cpu || ! soc || ! board )
        {
            log.fail( "Please specify 'cpu', 'soc' and 'board' parameters for compilation." )
            return Promise.reject()
        }

        // compiler configuration: commands, arguments, options etc.
        config.mergeA( zutil.require( 'config/compile/' + cpu + '.config' ) )

        // app specific overrides of global zcode project options
        config.mergeA( zutil.require( 'config/app/app.config' ) )
        // source files to compile
        config.mergeA( zutil.require( 'config/app/sources.config' ) )

        config.finalize()

        compile_commands = mr.processFilesForMap( config, '.app' )
    }

    return executeCommands( compile_commands )
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

export default function( zbuild )
{
    log = zbuild.log
    executeCommands = cmdrunner( zbuild )
    mr = mapreducefiles( zbuild )

    return commandCompile
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
