'use strict'

/*
    Kind of based on grunt.config
    https://github.com/gruntjs/grunt/blob/master/lib/grunt/config.js
*/

const _ = require( 'lodash' )
const getobject = require( 'getobject' )
const util = require( 'util' )


function Config( data )
{
    //
    // Case when Config is called as a function
    //
    if( ! ( this instanceof Config ) )
    {
        return new Config( data )
    }

    var _data = {}

    // Match '<%= FOO %>' where FOO is a propString, eg. foo or foo.bar but not
    // a method call like foo() or foo.bar().
    var _propStringTmplRe = /^<%=\s*([a-z0-9_$]+(?:\.[a-z0-9_$]+)*)\s*%>$/i;


    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    var kindsOf = {}
    'Number String Boolean Function RegExp Array Date Error'.split(' ').forEach( function( k )
    {
        kindsOf[ '[object ' + k + ']' ] = k.toLowerCase()
    })

    var kindOf = function( value )
    {
        // Null or undefined
        if( value == null )
        {
            return String(value)
        }

        return kindsOf[ kindsOf.toString.call( value ) ] || 'object'
    }


    const _recurse = function( value, fn )
    {
        var recurse_stateful = function( value, fn, state )
        {
            if( state.objs.indexOf( value ) !== -1 )
            {
                let error = new Error( 'Circular reference detected (' + state.path + ')' )
                error.path = state.path
                throw error
            }

            var obj
            var key

            if( Array.isArray( value ) )
            {
                return value.map( function( item, index )
                {
                    return _recurse( item, fn, {
                        objs: state.objs.concat( [ value ] ),
                        path: state.path + '[' + index + ']',
                    })
                })
            }
            else if ( kindOf( value ) === 'object' && !Buffer.isBuffer( value ) )
            {
                // If value is an object, recurse.
                obj = {}
                
                for( key in value )
                {
                    obj[key] = _recurse( value[key], fn,
                    {
                        objs: state.objs.concat( [ value ] ),
                        path: state.path + ( /\W/.test( key ) ? '["' + key + '"]' : '.' + key ),
                    })
                }
                
                return obj
            }
            else
            {
                // Otherwise pass value into fn and return.
                return fn( value )
            }
        }

        // objs - used to track already processed objects to detect circular references
        // path - used as a reference in error message
        return recurse_stateful( value, fn, { objs: [], path: '' } )
    }




    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    // Escape any . in name with \. so dot-based namespacing works properly.
    const _escape = function( str )
    {
        return str.replace(/\./g, '\\.')
    }


    // Return prop as a string.
    const _getPropString = function( prop )
    {
      return Array.isArray( prop ) ? prop.map( _escape ).join('.') : prop
    }


    // Get raw, unprocessed config data.
    const _getRaw = function( prop )
    {
        if( prop )
        {
            // Prop was passed, get that specific property's value.
            return getobject.get( _data, _getPropString( prop ) )
        }
        else
        {
            // No prop was passed, return the entire _data object.
            return _data
        }
    }

    // Get config data, recursively processing templates.
    const _get = function( prop )
    {
        return _process( _getRaw( prop ) )
    }

    // Expand a config value recursively. Used for post-processing raw values
    // already retrieved from the config.
    const _process = function( raw )
    {
        return _recurse(raw, function(value)
        {
            // If the value is not a string, return it.
            if( typeof value !== 'string' )
            {
                return value
            }
            
            var matches = value.match( _propStringTmplRe )

            if( matches )
            {
                // If possible, access the specified property via config.get, in case it
                // doesn't refer to a string, but instead refers to an object or array.
                var result = _get( matches[ 1 ] )
                if( result != null )
                {
                    return result
                }
            }

            // Process the string as a template.
            return _.template( value )( _data )
        })
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    // Set config data.
    const _set = function( prop, value )
    {
        return getobject.set( _data, _getPropString( prop ), value )
    }

    // Deep merge config data.
    const _merge = function( obj )
    {
        if( obj instanceof Config )
        {
            obj = obj.data
        }

        _.merge( _data, obj )

        return _data
    }

    const _mergeArrayUnion = function( obj )
    {
        function customizer( objValue, srcValue )
        {
            if( _.isArray( objValue ) )
            {
                return _.union( objValue, srcValue )
            }
        }

        if( obj instanceof Config )
        {
            obj = obj.data
        }

        _.mergeWith( _data, obj, customizer )

        return _data
    }

    // Initialize config data.
    const _init = function( data )
    {
        // Initialize and return data.
        return ( _data = data || {} )
    }


    // Test to see if required config params have been defined.
    // If not, throw an exception.
    const _requires = function()
    {
        var props = Array.prototype.slice.call( arguments ).map( _getPropString )

        var failProps = _data &&
            props.filter( function( prop )
            {
                return _get(prop) == null
            }).map( function( prop )
            {
                return '"' + prop + '"'
            })

        if( _data && failProps.length === 0 )
        {
            return true
        }
        else
        {
            throw new Error('Required config prop(s) ' + failProps.join(', ') + ' missing.')
        }
    }

    const _finalize = function()
    {
        _data = _process( _data )
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        data:
        {
            get: function() { return _data }
        }
    })


    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // CONSTRUCTOR CODE
    //
    _init( data )

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    this.get = _get
    this.set = _set
    this.merge = _merge
    this.mergeA = _mergeArrayUnion
    this.requires = _requires
    this.finalize = _finalize
}

//
// PUBLIC CLASS METHODS
//
Object.assign( Config.prototype,
{
    toString: function()
    {
        return util.inspect( this.data, false, null )
    },
} )

export default Config
