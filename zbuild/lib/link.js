'use strict'

import * as zutil from './util.js'
import Config from'./config.js'

import cmdrunner from './cmdrunner.js'
import mapreducefiles from './mapreducefiles.js'


//////////////////////////////////////////////////////////////////////////////////////////////////////

var mr = null
var executeCommands = null
var log = null

//////////////////////////////////////////////////////////////////////////////////////////////////////

const commandLink = function( config, options )
{
    let link_commands = []

    if( options.zcode )
    {
        let cpu = options.cpu
        let soc = options.soc
        let board = options.board

        if( ! cpu || ! soc || ! board )
        {
            log.fail( "Please specify 'cpu', 'soc' and 'board' parameters for linking." )
            return Promise.reject()
        }

        // to link 'zcode' we need some options from compile section (destination i.e.)
        config.mergeA( zutil.require( 'config/compile/zcode.options.config' ) )

        // linker configuration: commands, arguments, options etc.
        config.mergeA( zutil.require( 'config/link/zcode.config' ) )

        // source files to link to target
        config.mergeA( zutil.require( 'config/sources/compiler.config' ) )
        config.mergeA( zutil.require( 'config/sources/zcode.config' ) )
        config.mergeA( zutil.require( 'config/sources/cpu/' + cpu + '.config' ) )
        config.mergeA( zutil.require( 'config/sources/soc/' + soc + '.config' ) )
        config.mergeA( zutil.require( 'config/sources/board/' + board + '.config' ) )

        config.finalize()

        link_commands = mr.processFilesForReduce( config, '.zcode' )
    }
    else if( options.vendor )
    {
        let variant = options.variant
        let vname = options.vname

        if( ! variant )
        {
            log.fail( "Please specify 'variant' parameter for linking." )
            return Promise.reject()
        }

        // link may depend on some settings from compile stage, i.e. sources location
        config.mergeA( zutil.require( 'config/vendor/' + vname + '/' + variant + '.compile.config' ) )
        // link depends on list of source files
        config.mergeA( zutil.require( 'config/vendor/' + vname + '/' + variant + '.sources.config' ) )
        config.mergeA( zutil.require( 'config/vendor/' + vname + '/' + variant + '.link.config' ) )

        config.finalize()

        link_commands = mr.processFilesForReduce( config, '.' + variant )
    }
    else if( options.app )
    {
        // UNFIXME!!
        let vname = 'lpc_open'
        let variant = 'lpc_chip_13xx'

        // app specific overrides of global zcode project options
        config.mergeA( zutil.require( 'config/app/app.config' ) )

        let cpu = options.cpu
        let soc = options.soc
        let board = options.board

        if( ! cpu || ! soc || ! board )
        {
            log.fail( "Please specify 'cpu', 'soc' and 'board' parameters for linking." )
            return Promise.reject()
        }

        // zcode.config depenes on zcode.options.config
        config.mergeA( zutil.require( 'config/compile/zcode.options.config' ) )
        // '.link.config' depenes on '.compile.config'
        config.mergeA( zutil.require( 'config/vendor/' + vname + '/' + variant + '.compile.config' ) )

        // pick up various definitions and info from link config files
        config.mergeA( zutil.require( 'config/link/zcode.config' ) )
        config.mergeA( zutil.require( 'config/link/' + cpu + '/' + soc + '/link.options.config' ) )
        config.mergeA( zutil.require( 'config/vendor/' + vname + '/' + variant + '.link.config' ) )

        // linker configuration: commands, arguments, options etc.
        config.mergeA( zutil.require( 'config/app/link.config' ) )


        // files to link to target
        config.mergeA( zutil.require( 'config/app/sources.config' ) )

        config.finalize()

        link_commands = mr.processFilesForReduce( config, '.app' )
    }

    return executeCommands( link_commands )
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

export default function( zbuild )
{
    log = zbuild.log
    executeCommands = cmdrunner( zbuild )
    mr = mapreducefiles( zbuild )

    return commandLink
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
