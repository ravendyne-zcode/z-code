'use strict'

const path = require( 'path' )

const baseFolder = process.cwd()

const _require = function( location )
{
    return require( path.resolve( path.join( baseFolder, location ) ) )
}

export { _require as require }
export { baseFolder }
