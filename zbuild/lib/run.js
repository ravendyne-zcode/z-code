'use strict'

const _ = require( 'lodash' )

import * as zutil from './util.js'

import cmdrunner from './cmdrunner.js'

//////////////////////////////////////////////////////////////////////////////////////////////////////

var executeCommands = null
var log = null

//////////////////////////////////////////////////////////////////////////////////////////////////////

const commandRun = function( config, options )
{
    let commands_to_execute = []

    config.mergeA( zutil.require( 'config/run.commands.config' ) )
    config.finalize()

    let configBase = 'commands.' + ( options.name || 'default' )
    config.requires( configBase + '.cmd' )
    config.requires( configBase + '.args' )

    //
    // get config values
    //
    let alwaysShowCmdOutput = config.get( configBase + '.alwaysShowCmdOutput' ) || true
    let cmd = config.get( configBase + '.cmd' )
    let args = config.get( configBase + '.args' )
    let echo = config.get( configBase + '.echo' ) || 'RUN'

    args = _.compact( _.flatten( args ) )

    commands_to_execute = [{
        cmd: cmd,
        args: args,
        echo: echo,
        alwaysShowCmdOutput: alwaysShowCmdOutput,
    }]

    return executeCommands( commands_to_execute )
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

export default function( zbuild )
{
    log = zbuild.log
    executeCommands = cmdrunner( zbuild )

    return commandRun
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
