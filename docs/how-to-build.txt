. bin/zenv.sh

zbuild clean
zbuild clean --force

zbuild --vendor --vname=lpc_open --variant=lpc_chip_13xx compile
zbuild --vendor --vname=lpc_open --variant=lpc_chip_13xx link

zbuild --zcode --cpu=cortex-m3 --soc=qemu --board=lpcxpresso compile
zbuild --zcode --cpu=cortex-m3 --soc=qemu --board=lpcxpresso link

zbuild --app --cpu=cortex-m3 --soc=qemu --board=lpcxpresso compile
zbuild --app --cpu=cortex-m3 --soc=qemu --board=lpcxpresso link

zbuild run --name=<literal name>

zbuild run --name=qemu
qemu-system-arm -M lm3s6965evb -cpu cortex-m3 -no-reboot -serial stdio -kernel build/app/zcode_app.axf

# TODO
zbuild build all --cpu=cortex-m3 --soc=qemu --board=lpcxpresso --variant=lpc_chip_13xx --vname=lpc_open


--------------------------------------



[postbuild]

1:
    arm-none-eabi-size "lib${BuildArtifactFileName}"

2:
    arm-none-eabi-objdump -h -S "lib${BuildArtifactFileName}" > "${BuildArtifactFileBaseName}.lss"

3:
    arm-none-eabi-objdump -a liblpc1347.a | grep "file"
    arm-none-eabi-nm liblpc1347.a | grep ":"


arm-none-eabi-objdump -h -S "build/zcode/libzcode_cortex-m3_lpc1347_lpcxpresso.a" > "build/zcode/libzcode_cortex-m3_lpc1347_lpcxpresso.a.lss"

arm-none-eabi-objdump -a build/zcode/libzcode_cortex-m3_lpc1347_lpcxpresso.a | grep "file"
arm-none-eabi-nm build/zcode/libzcode_cortex-m3_lpc1347_lpcxpresso.a | grep ":"



arm-none-eabi-size "zcode_dummy.axf"
arm-none-eabi-objcopy -v -O binary "zcode_dummy.axf" "zcode_dummy.bin"

checksum -p lpc1347 -d "zcode_dummy.bin"

--------------------------------------

https://community.nxp.com/thread/389139


export PATH=$PATH:/usr/local/lpcxpresso_8.1.4_606/lpcxpresso/bin:/usr/local/lpcxpresso_8.1.4_606/lpcxpresso/tools/bin

# boot up LPCLink
boot_link1


# erase flash
crt_emu_cm_redlink -flash-mass-erase --rst -g -2 -vendor=NXP -plpc1347

crt_emu_cm_redlink -flash-mass -g -2  -vendor=NXP -pLPC1347    -reset=vectreset -flash-driver=LPC11_12_13_64K_8K.cfx



# write program to flash
crt_emu_cm_redlink -flash-load-exec "build/app/zcode_app.axf" --rst -g -2 -vendor=NXP -plpc1347 -reset=vectreset -flash-driver=LPC11_12_13_64K_8K.cfx

crt_emu_cm_redlink -flash-load-exec "build/app/zcode_app.axf" -g -2  -vendor=NXP -pLPC1347 -reset=vectreset -flash-driver=LPC11_12_13_64K_8K.cfx

-reset=sysresetreq
-reset=hard


arm-none-eabi-objdump -d build/app/zcode_app.axf> build/app/zcode_app.axf.S


crt_emu_cm_redlink -flash-mass -g -2 -vendor=NXP -pLPC1347 -reset=vectreset -flash-driver=LPC11_12_13_64K_8K.cfx
crt_emu_cm_redlink -flash-load-exec zcode-dummy.axf --rst -g -2 -vendor=NXP -plpc1347 -reset=vectreset -flash-driver=LPC11_12_13_64K_8K.cfx

