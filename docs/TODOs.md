- soc -> lpc1347 depends on vendor -> lpc_open -> lpc_chip_13xx to compile
    - [ ] add dependency mechanism for config file(s) to pull in options (i.e. INCLUDES)

- only changed files: https://github.com/gruntjs/grunt-contrib-copy/issues/78#issuecomment-19021639


- [ ] separate app file sources into its own directory

- [ ] create clean vendor <- zcode <- app dependency
    * zcode
        - vendor includes
    * vendor
        - none
    * app
        - zcode includes
        - zcode library name
        - vendor library name
        - zcode library path
        - vendor library path


- [ ] replace throw statement in delegate.hpp with some sort of error handler that signals there's been an error in code execution

- [x] configure all pins for lpcxpresso board
- [x] instantiate all digital pins on HAL level

- [x] clean out SPIContext send/receive process handling logic
- [x] pin mode set structure for GPIO, ADC, PWM etc.
- [x] implement ADC pins.
- [ ] SSP -> when read() is issued and previous command was write(), empty Rx FIFO before writing dummy byte to Tx FIFO
    otherwise read() returns whatever was left in Rx FIFO from write() operation(s)
- [x] implement hal::Spi::initialize()


- [ ] make gpio::Pin use dummy hal pin implementation if halPinNumber is wrong (and/or report an error).
    * then, remove halPinNumber checks in hal::Pin implementation.
- [ ] convert gpio::Pin/gpio::hal::Pin combo to follow composition pattern instead of inheritance.
- [ ] create gpio::FastPin interface to be implemented on HAL level




==========================================================================================================
zbuild
----------------------------------------------------------------------------------------------------------

- [ ] add command array filtering in compile.js/link.js before sending it to cmdrunner
    * filter out non-modified files
    * delete output files before handing off to cmdRunner ???


