// http://www.nxp.com/docs/en/user-guide/UM10204.pdf

// http://www.ti.com/lit/an/slva704/slva704.pdf
// http://i2c.info/i2c-bus-specification
// https://en.wikipedia.org/wiki/I%C2%B2C


#define I2C_CON_FLAGS (I2C_CON_AA | I2C_CON_SI | I2C_CON_STO | I2C_CON_STA)


/* I2C common interface structure */
struct i2c_interface {
    LPC_I2C_T *ip;      /* IP base address of the I2C device */
    CHIP_SYSCTL_CLOCK_T clk;    /* Clock used by I2C */
    I2C_EVENTHANDLER_T mEvent;  /* Current active Master event handler */
    I2C_EVENTHANDLER_T sEvent;  /* Slave transfer events */
    I2C_XFER_T *mXfer;  /* Current active xfer pointer */
    I2C_XFER_T *sXfer;  /* Pointer to store xfer when bus is busy */
    uint32_t flags;     /* Flags used by I2C master and slave */
};


/* I2C interfaces */
static struct i2c_interface i2c[I2C_NUM_INTERFACE] = {
    {LPC_I2C, SYSCTL_CLOCK_I2C, Chip_I2C_EventHandler, NULL, NULL, NULL, 0}
};



/* Get current state of the I2C peripheral */
STATIC INLINE int getCurState(LPC_I2C_T *pI2C)
{
    return (int) (pI2C->STAT & I2C_STAT_CODE_BITMASK);
}

/**
 * @brief Master transfer data structure definitions
 */
typedef struct {
    uint8_t slaveAddr;      /**< 7-bit I2C Slave address */
    const uint8_t *txBuff;  /**< Pointer to array of bytes to be transmitted */
    int     txSz;           /**< Number of bytes in transmit array,
                               if 0 only receive transfer will be carried on */
    uint8_t *rxBuff;        /**< Pointer memory where bytes received from I2C be stored */
    int     rxSz;           /**< Number of bytes to received,
                               if 0 only transmission we be carried on */
    I2C_STATUS_T status;    /**< Status of the current I2C transfer */
} I2C_XFER_T;

/* Master transfer state change handler handler */
int handleMasterXferState(LPC_I2C_T *pI2C, I2C_XFER_T  *xfer)
{
    // initially, CLEAR ALL bits:
    // START, STOP, AA, SI
    // cclr -> if bit is SET, I2C_CON flag will be CLEARED
    uint32_t cclr = I2C_CON_FLAGS;

    // I2C_CON_AA -> when in master mode:
        // - if SET, ACK will be sent when data is received
        // - if CLEAR, NACK will be sent when data is received
    // I2C_CON_SI -> clear on every IRQ service
    // I2C_CON_STO -> when in master mode: send STOP
    // I2C_CON_STA -> switch to master and send START, if already master send Repeated-START

    switch (getCurState(pI2C)) {
    case 0x08:      /* Start condition on bus */
    case 0x10:      /* Repeated start condition */
        pI2C->DAT = (xfer->slaveAddr << 1) | (xfer->txSz == 0);
        break;

    /* Tx handling */
    case 0x18:      /* SLA+W sent and ACK received */
    case 0x28:      /* DATA sent and ACK received */
        if (!xfer->txSz) {
            // NOT transmitting case:
            // if no more data to Rx, clear STOP bit in cclr -> send STOP
            // if more data to Rx, clear START bit in cclr -> send (Re)START
            cclr &= ~(xfer->rxSz ? I2C_CON_STA : I2C_CON_STO);
        }
        else {
            // transmitting case:
            // read DAT
            pI2C->DAT = *xfer->txBuff++;
            xfer->txSz--;
        }
        break;

    /* Rx handling */
    case 0x58:      /* Data Received and NACK sent */
        // clear STOP bit in cclr -> send STOP
        cclr &= ~I2C_CON_STO;

    case 0x50:      /* Data Received and ACK sent */
        *xfer->rxBuff++ = pI2C->DAT;
        xfer->rxSz--;

    case 0x40:      /* SLA+R sent and ACK received */
        if (xfer->rxSz > 1) {
            // if this is not the next-to-last (or the last) byte that we just received
            // clear AA bit in cclr -> send ACK
            // otherwise we will mark NACK to be sent when we receive next-to-last byte
            // which will set I2C state to 0x58 when we receive the last byte
            cclr &= ~I2C_CON_AA;
        }
        break;

    /* NAK Handling */
    case 0x20:      /* SLA+W sent NAK received */
    case 0x30:      /* DATA sent NAK received */
    case 0x48:      /* SLA+R sent NAK received */
        xfer->status = I2C_STATUS_NAK;
        // clear STOP bit in cclr -> send STOP
        cclr &= ~I2C_CON_STO;
        break;

    case 0x38:      /* Arbitration lost */
        xfer->status = I2C_STATUS_ARBLOST;
        break;

    /* Bus Error */
    case 0x00:
        xfer->status = I2C_STATUS_BUSERR;
        // clear STOP bit in cclr -> send STOP
        cclr &= ~I2C_CON_STO;
    }

    /* Set clear control flags */
    pI2C->CONSET = cclr ^ I2C_CON_FLAGS;
    pI2C->CONCLR = cclr;

    /* If stopped return 0 */
    if (!(cclr & I2C_CON_STO) || (xfer->status == I2C_STATUS_ARBLOST)) {
        if (xfer->status == I2C_STATUS_BUSY) {
            xfer->status = I2C_STATUS_DONE;
        }
        return 0;
    }
    return 1;
}











/* Enable I2C and start master transfer */
STATIC INLINE void startMasterXfer(LPC_I2C_T *pI2C)
{
    /* Reset STA, STO, SI */
    pI2C->CONCLR = I2C_CON_SI | I2C_CON_STO | I2C_CON_STA | I2C_CON_AA;

    /* Enter to Master Transmitter mode */
    pI2C->CONSET = I2C_CON_I2EN | I2C_CON_STA;
}






/**
 * @brief   I2C Interrupt Handler
 * @return  None
 */
void I2C_IRQHandler(void)
{
    if (!handleMasterXferState(i2c[I2C0].ip, i2c[I2C0].mXfer))
    {
        // if I2C all done then signal DONE event
        // Chip_I2C_EventHandler(I2C0, I2C_EVENT_DONE)
        i2c[I2C0].mEvent(I2C0, I2C_EVENT_DONE);
    }

}




//---- INIT
Init_I2C_PinMux();

/* Initialize I2C */
Chip_I2C_Init(I2C0);
Chip_I2C_SetClockRate(I2C0, SPEED_100KHZ);



NVIC_EnableIRQ(I2C0_IRQn);


// do some work here...

Chip_I2C_DeInit(I2C0);
