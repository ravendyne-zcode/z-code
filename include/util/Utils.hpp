#pragma once
#ifndef UTILS_HPP_INCLUDED
#define UTILS_HPP_INCLUDED

#include <cstdint>
#include <types>

namespace util
{

    unsigned char *strrev( unsigned char *str, int len );

    int itoa( int num, int base, char* str, int len );

    inline int toHex( int num, char* str, int len ) { return itoa( num, 16, str, len ); };

    int ftoa( float num, int precision, char* str, int len );

}

#endif // UTILS_HPP_INCLUDED
