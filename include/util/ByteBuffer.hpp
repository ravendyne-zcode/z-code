#pragma once
#ifndef BYTEBUFFER_HPP_INCLUDED
#define BYTEBUFFER_HPP_INCLUDED

#include <cstdint>
#include <types>

namespace util
{
    // This is essentially a shorthand for (uint8_t* buffer, int lenght)
    // so we can just pass one argument to many different methods in the
    // code base instead of dragging two arguments around.
    // It also may contain some handy methods for working with byte buffers.
    class ByteBuffer
    {
    public:
        ByteBuffer( uint8_t* buffer, uint32_t length );

        uint8_t& operator[]( uint32_t idx ) { return this->buffer[ idx ]; }
        const uint8_t& operator[]( uint32_t idx ) const { return this->buffer[ idx ]; }

        uint32_t length;
        uint8_t* buffer;
    };
}

#endif // BYTEBUFFER_HPP_INCLUDED
