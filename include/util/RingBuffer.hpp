#pragma once
#ifndef RINGBUFFER_HPP_INCLUDED
#define RINGBUFFER_HPP_INCLUDED

#include <cstdint>
#include <util/RingBuffer.hpp>
#include <util/ByteBuffer.hpp>

namespace util
{
    class RingBuffer
    {
    public:
        // length -> buffer length in bytes
        // itemSize -> sizeof( one element )
        RingBuffer( void *buffer, uint32_t length, uint32_t itemSize );
        RingBuffer( ByteBuffer &buffer, uint32_t itemSize );

        // Use this constructor and init() method to avoid constructing RingBuffer with 'new'
        RingBuffer();

        // length -> buffer length in bytes
        // itemSize -> sizeof( one element )
        void init( void *buffer, uint32_t length, uint32_t itemSize );

        bool isEmpty();

        bool isFull();

        uint32_t availableData();

        uint32_t availableSpace();

        uint32_t push( const void *data, uint32_t itemCount );

        uint32_t pushOne( const void *data );

        uint32_t pop( void *dataBuffer, uint32_t itemCount );

        uint32_t popOne( void *dataBuffer );

        uint32_t peek( void *dataBuffer );

        void flush();

        //void destroy();

    private:
        uint8_t *_buffer;
        uint32_t _length;
        uint32_t _itemCount;
        uint32_t _itemSize;
        uint32_t _head;
        //uint32_t tail; // we use fill count to track position

        uint32_t capacity();

        // We need this one since we don't keep track of tail
        uint32_t tail();

        // This one is just for consistency sake
        uint32_t head();

        void* headPtr();

        void* tailPtr();

        void headAdvance( uint32_t count );

        void headAdvanceOne();

        void tailAdvance( uint32_t count );

        void tailAdvanceOne();

        void* pushUnchecked( const void *data, uint32_t itemCount );

        void* popUnchecked( void *data, uint32_t itemCount );

        uint32_t spaceBetweenTailAndEnd();
    };
}

#endif // RINGBUFFER_HPP_INCLUDED
