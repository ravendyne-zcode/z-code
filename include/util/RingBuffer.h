// This file is a part of the zcore - www.zeeduino.com
//
// Copyright 2017 Ravendyne Inc
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef RINGBUFFER_H_INCLUDED
#define RINGBUFFER_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    void *buffer;
    uint32_t length;
    uint32_t itemCount;
    uint32_t itemSize;
    uint32_t head;
    //uint32_t tail; // we use fill count to track position
} RingBuffer;

void RingBuffer_init(RingBuffer *rbuff, void *buffer, uint32_t count, uint32_t itemSize);

bool RingBuffer_isEmpty(RingBuffer *rbuff);

bool RingBuffer_isFull(RingBuffer *rbuff);

uint32_t RingBuffer_availableData(RingBuffer *rbuff);

uint32_t RingBuffer_availableSpace(RingBuffer *rbuff);

uint32_t RingBuffer_push(RingBuffer *rbuff, const void *data, uint32_t itemCount);

uint32_t RingBuffer_pushOne(RingBuffer *rbuff, const void *data);

uint32_t RingBuffer_pop(RingBuffer *rbuff, void *dataBuffer, uint32_t itemCount);

uint32_t RingBuffer_popOne(RingBuffer *rbuff, void *dataBuffer);

uint32_t RingBuffer_peek(RingBuffer *rbuff, void *dataBuffer);

void RingBuffer_flush(RingBuffer *rbuff);

//void RingBuffer_destroy(RingBuffer *buffer);

#ifdef __cplusplus
}
#endif

#endif // RINGBUFFER_H_INCLUDED
