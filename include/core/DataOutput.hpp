#pragma once
#ifndef DATAOUTPUT_HPP_INCLUDED
#define DATAOUTPUT_HPP_INCLUDED

#include <types>
#include <string>

#include <util/ByteBuffer.hpp>

namespace zcore
{

    class DataOutput
    {
    public:
        virtual void    write( int value ) = 0;
        virtual void    write( long value ) = 0;
        virtual void    write( short value ) = 0;
        virtual void    write( uint8_t value ) = 0;
        virtual void    write( uint16_t value ) = 0;
        virtual void    write( uint32_t value ) = 0;

        virtual void    write( double value ) = 0;
        virtual void    write( float value ) = 0;
        virtual void    write( bool value ) = 0;

        virtual void    write( char value ) = 0;
        virtual void    write( String &str ) = 0;
        virtual void    write( const char* string ) = 0;
    };

}

#endif // DATAOUTPUT_HPP_INCLUDED
