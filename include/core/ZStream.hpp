#pragma once
#ifndef ZSTREAM_HPP_INCLUDED
#define ZSTREAM_HPP_INCLUDED

/*! \file ZStream.hpp
    \brief Wiring/Arduino compatible stream class
*/

#include <zprint>
#include <string>

//! Char and byte input stream processing. Needs to be subclassed.
/*!
    Provides implementation of:

    - searching for specified pattern in the stream, skipping anything:
        + until timeout
        + until specified number of chars/bytes is processed
        + until specified terminator is found
    - reading data:
        + up to a specified length
        + up to a specified terminator pattern

    This class is mainly used on serial input,
    but could also be used for char and byte buffers.
*/
class ZStream : public ZPrint
{
public:
    ZStream();

    virtual size_t available() = 0;
    virtual size_t read() = 0;
    virtual size_t peek() = 0;
    virtual void flush() = 0;

    void setTimeout(unsigned long timeout);

    //! Reads (and discards) data from the stream up until the start of the given pattern
    /*! All characters/bytes read from the stream by this method are discarded.
        \param pattern '\0'-terminated char array that will be used for pattern
        \return true if pattern was found, false if end-of-stream was reached or timeout occures before the pattern was found
    */
    bool find(char *pattern);

    //! **For Wiring/Arduino compatibility.** For byte streams use find(uint8_t *, size_t)
    /*! Works the same as find(char *) assuming that pattern is the pointer to '\0'-terminated char array.
        Not really the safest thing to assume...
    */
    bool find(uint8_t *pattern);

    //! **For Wiring/Arduino compatibility.** For char streams use find(char *)
    bool find(char *pattern, size_t length);
    
    //! Reads (and discards) data from the stream up until the start of the given pattern
    /*! All characters/bytes read from the stream by this method are discarded.
        \param pattern byte array that will be used for pattern
        \param length lenght of the pattern byte array
        \return true if pattern was found, false if end-of-stream was reached or timeout occures before the pattern was found
    */
    bool find(uint8_t *pattern, size_t length);

    //! Reads (and discards) data from the stream up until the start of the given pattern. Encountering terminator ends the search.
    /*! All characters/bytes read from the stream by this method are discarded.
        \param pattern '\0'-terminated char array that will be used for pattern
        \param terminator '\0'-terminated char array that will stop the search, if found
        \return true if pattern was found, false if end-of-stream was reached or timeout occures before the pattern was found
    */
    bool findUntil(char *pattern, char *terminator);

    //! **For Wiring/Arduino compatibility.** For char streams use find(char *)
    bool findUntil(uint8_t *pattern, char *terminator);

    //! **For Wiring/Arduino compatibility.** Use find(char*, char*) for char streams.
    /*! Works the same as findUntil(uint8_t *, size_t , char *, size_t ).
        Assumes patternLen = strlen(pattern) and termLen = strlen(terminator).
    */
    bool findUntil(char *pattern, size_t patternLen, char *terminator, size_t termLen);

    //! Reads (and discards) data from the stream up until the start of the given pattern. Encountering terminator ends the search.
    /*! All characters/bytes read from the stream by this method are discarded.
        \param pattern byte array that will be used for pattern
        \param patternLen lenght of the pattern byte array
        \param terminator byte array that will be used for terminator
        \param termLen lenght of the terminator byte array
        \return true if pattern was found, false if end-of-stream was reached or terminator pattern was encountered
        or timeout occures before the pattern was found
    */
    bool findUntil(uint8_t *pattern, size_t patternLen, uint8_t *terminator, size_t termLen);


    //! Parses an integer from the stream.
    /*! The parsing stops as soon as character other than '0'-'9' is encountered.
        If input characters are representing a number that falls out of 32-bit range, the result returned by this method is undefined.
        \return parsed int on success, 0 on failure.
    */
    long parseInt();

    //! Parses a fixed point decimal number from the stream.
    /*! The parsing stops as soon as character other than '0'-'9' or '.' is encountered. Parsing also stops if second '.' is encountered.
        If input characters are representing a number that falls out of range for 32-bit IEEE floating point representation,
        the result returned by this method is undefined.
        \return parsed int on success, NaN value on failure.
    */
    float parseFloat();

    //! Brief.
    size_t readBytes(char *buffer, size_t length);
    //! Brief.
    size_t readBytes(uint8_t *buffer, size_t length);

    //! Brief.
    size_t readBytesUntil(char terminator, char *buffer, size_t length);
    //! Brief.
    size_t readBytesUntil(char terminator, uint8_t *buffer, size_t length);

    //! Brief.
    zcore::String readString();
    //! Brief.
    zcore::String readStringUntil(char terminator);

};


#endif // ZSTREAM_HPP_INCLUDED
