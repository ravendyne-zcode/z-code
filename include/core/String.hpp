#pragma once
#ifndef STRING_HPP_INCLUDED
#define STRING_HPP_INCLUDED

namespace zcore
{

    class String
    {
    public:
        String();
        String( const char* buffer );
    };

}

#endif // STRING_HPP_INCLUDED
