#pragma once
#ifndef BOARD_LED_HPP_INCLUDED
#define BOARD_LED_HPP_INCLUDED

#include <core/soc/Pin.hpp>

namespace board
{
    class Led : public gpio::hal::Pin
    {
    public:
        Led( int halPinNumber );

        void on();
        void off();
    };

    Led* getLed( uint8_t ledNumber );
}

#endif // BOARD_LED_HPP_INCLUDED
