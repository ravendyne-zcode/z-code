#pragma once
#ifndef BOARD_HPP_INCLUDED
#define BOARD_HPP_INCLUDED

namespace board
{
    void setupClocking();
    void initialize();

    void setupPins();
}

#endif // BOARD_HPP_INCLUDED
