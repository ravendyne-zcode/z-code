#pragma once
#ifndef BOARD_BUTTON_HPP_INCLUDED
#define BOARD_BUTTON_HPP_INCLUDED

#include <core/soc/Pin.hpp>
#include <util/delegate.hpp>

namespace board
{
    class Button : public gpio::hal::Pin
    {
    public:
        Button( int buttonNumber, int halPinNumber );

        bool pressed();

        using ButtonChangeCallback = delegate< void ( int buttonNumber ) >;
        void onChange( ButtonChangeCallback callback );

    private:
        int buttonNumber;
    };

    Button* getButton( int buttonNumber );
}

#endif // BOARD_BUTTON_HPP_INCLUDED
