#pragma once
#ifndef BOARD_STANDARD_DEVICES_HPP_INCLUDED
#define BOARD_STANDARD_DEVICES_HPP_INCLUDED

#include "core/board/Led.hpp"
#include "core/board/Button.hpp"

extern board::Led LED;
extern board::Button Button;

#endif // BOARD_STANDARD_DEVICES_HPP_INCLUDED
