#pragma once
#ifndef DATAINPUT_HPP_INCLUDED
#define DATAINPUT_HPP_INCLUDED

#include <types>

#include <util/ByteBuffer.hpp>

namespace zcore
{

    class DataInput
    {
    public:
        virtual int8_t   readByte() = 0;
        virtual int      readInt() = 0;
        virtual long     readLong() = 0;
        virtual short    readShort() = 0;
        virtual uint8_t  readUnsignedByte() = 0;
        virtual uint16_t readUnsignedShort() = 0;
        virtual uint32_t readUnsignedLong() = 0;

        virtual double   readDouble() = 0;
        virtual float    readFloat() = 0;
        virtual bool     readBoolean() = 0;

        virtual char     readChar() = 0;
        virtual int      readLine( util::ByteBuffer &buffer ) = 0;
        virtual int      readLine( char* buffer, int maxLength ) = 0;

        virtual int      skipBytes( int n ) = 0;
    };

}

#endif // DATAINPUT_HPP_INCLUDED
