#pragma once
#ifndef SYSTEM_SYSTEMTICK_HPP_INCLUDED
#define SYSTEM_SYSTEMTICK_HPP_INCLUDED

namespace system
{
    void SystemTickHandler();
}

#endif // SYSTEM_SYSTEMTICK_HPP_INCLUDED
