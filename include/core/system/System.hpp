#pragma once
#ifndef SYSTEM_SYSTEM_HPP_INCLUDED
#define SYSTEM_SYSTEM_HPP_INCLUDED

namespace system
{
    void yield();
}

#endif // SYSTEM_SYSTEM_HPP_INCLUDED
