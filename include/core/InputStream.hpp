#pragma once
#ifndef INPUTSTREAM_HPP_INCLUDED
#define INPUTSTREAM_HPP_INCLUDED

#include <types>

#include <util/ByteBuffer.hpp>

namespace zcore
{

    class InputStream
    {
    public:
        virtual int     available() { return 0; };
        virtual void    close() {};

        virtual long    skip( long n );
        virtual void    mark( int readlimit ) {};
        virtual bool    markSupported() { return false; };
        virtual void    reset() {};

        virtual bool    read( uint8_t* byte ) = 0;
        virtual int     read( util::ByteBuffer &buffer );
        virtual int     read( uint8_t* buffer, int length );
    };

}

#endif // INPUTSTREAM_HPP_INCLUDED
