#pragma once
#ifndef OUTPUTSTREAM_HPP_INCLUDED
#define OUTPUTSTREAM_HPP_INCLUDED

#include <types>

#include <util/ByteBuffer.hpp>

namespace zcore
{

    class OutputStream
    {
    public:
        virtual void    close() {};
        virtual void    flush() {};

        virtual int     write( util::ByteBuffer &buffer );
        virtual int     write( uint8_t* buffer, int length );
        virtual bool    write( uint8_t value ) = 0;
    };

}

#endif // OUTPUTSTREAM_HPP_INCLUDED
