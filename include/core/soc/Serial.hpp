#pragma once
#ifndef SOC_SERIAL_HPP_INCLUDED
#define SOC_SERIAL_HPP_INCLUDED

#include <types>

#include <streams>

namespace serial
{
    class Serial;

    namespace hal
    {
        void initialize();

        class UART : public zcore::InputStream, public zcore::OutputStream
        {
        };
    }

    class Serial //: public DataInput, public DataOutput
    {
    public:
        Serial( hal::UART* uart );

        bool write( char value );
        void write( const char *string );

        char readChar();
        int readLine( char *buffer, int maxLength );

    private:
        hal::UART* uart;
    };
}

#endif // SOC_SERIAL_HPP_INCLUDED
