#pragma once
#ifndef SOC_STANDARD_DEVICES_HPP_INCLUDED
#define SOC_STANDARD_DEVICES_HPP_INCLUDED

#include <serial>
#include <core_soc>
#include <gpio>

extern serial::Serial Serial;
extern spi::Spi Spi;
extern i2c::I2c I2c;

extern gpio::Pin pin_0;
extern gpio::Pin pin_1;
extern gpio::Pin pin_2;
extern gpio::Pin pin_3;
extern gpio::Pin pin_4;
extern gpio::Pin pin_5;
extern gpio::Pin pin_6;
extern gpio::Pin pin_7;
extern gpio::Pin pin_8;
extern gpio::Pin pin_9;
extern gpio::Pin pin_10;
extern gpio::Pin pin_11;
extern gpio::Pin pin_12;
extern gpio::Pin pin_13;
extern gpio::Pin pin_14;
extern gpio::Pin pin_15;

extern adc::Adc AD0;
extern adc::Adc AD1;
extern adc::Adc AD2;
extern adc::Adc AD3;
extern adc::Adc AD4;
extern adc::Adc AD5;


#endif // SOC_STANDARD_DEVICES_HPP_INCLUDED
