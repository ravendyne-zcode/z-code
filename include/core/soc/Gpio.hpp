#pragma once
#ifndef SOC_GPIO_HPP_INCLUDED
#define SOC_GPIO_HPP_INCLUDED

namespace gpio
{
    namespace hal
    {
        void initialize();
    }
}

#endif // SOC_GPIO_HPP_INCLUDED
