#pragma once
#ifndef SOC_HPP_INCLUDED
#define SOC_HPP_INCLUDED

namespace soc
{
    void initialize();
    void powerDown();
}

#endif // SOC_HPP_INCLUDED
