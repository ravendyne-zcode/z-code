#pragma once
#ifndef SOC_PIN_HPP_INCLUDED
#define SOC_PIN_HPP_INCLUDED

#include <types>
#include <util/delegate.hpp>

enum class PinDir
{
    Input,
    Output
};

enum class PinMode
{
    GPIO,
    ADC,
    PWM,
    UserDefined
};

namespace gpio
{
    namespace hal
    {
        struct PortPin
        {
            uint8_t port;
            uint8_t pin;
        };

        // Maps HAL pins to SOC level port/pin combination
        extern const PortPin HalPinMap[];
        extern const uint32_t halPinCount;
        inline int halPinToSocPort( uint32_t halPinNumber )
        {
            return halPinNumber <= halPinCount ? HalPinMap[ halPinNumber ].port : 0xFF;
        }
        inline int halPinToSocPin( uint32_t halPinNumber )
        {
            return halPinNumber <= halPinCount ? HalPinMap[ halPinNumber ].pin : 0xFF;
        }

        class Pin
        {
            const uint8_t NOT_A_HAL_PIN = 0xFF;

        public:
            Pin( int halPinNumber );
            Pin( int socPortNumber, int socPinNumber );

            void high();
            void low();
            void toggle();
            void mode( PinMode mode );
            void direction( PinDir direction );
            bool read();

            inline const uint8_t getPort() { return this->port; };
            inline const uint8_t getPin() { return this->pin; };

        private:
            uint8_t halPinNumber;
            uint8_t port;
            uint8_t pin;

            uint32_t getHalPinNumber();
        };
    }

    class Pin : public hal::Pin
    {
    public:
        Pin( uint32_t halPinNumber );

        bool isHigh();
        bool isLow();

        using ChangeCallback = delegate< void ( int value ) >;
        void onChange( ChangeCallback callback );
    };
}

#endif // SOC_PIN_HPP_INCLUDED
