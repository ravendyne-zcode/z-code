#pragma once
#ifndef SOC_I2C_HPP_INCLUDED
#define SOC_I2C_HPP_INCLUDED

#include <types>
#include <util/delegate.hpp>

#include <util/ByteBuffer.hpp>

namespace i2c
{
    namespace hal
    {
        void initialize();

        enum class Event
        {
            IDLE,
            START_SENT,
            DATA_RECEIVED,
            DATA_SENT_ACK_RECEIVED,
            DATA_SENT_NACK_RECEIVED,
            // DATA_SENT,
            // ACK_RECEIVED,
            // NACK_RECEIVED,
            ARBITRATION_LOST,
            BUS_ERROR,
        };

        class I2C
        {
        public:
            virtual void start() = 0;
            virtual void restart() = 0;
            virtual void stop() = 0;
            virtual void ack() = 0;
            virtual void nack() = 0;

            virtual void address( uint16_t address ) = 0;
            virtual void addressModeRead() = 0;
            virtual void addressModeWrite() = 0;

            virtual i2c::hal::Event lastEvent() = 0;

            virtual bool writeAddress() = 0;
            virtual bool write( uint8_t byte ) = 0;
            virtual bool read( uint8_t *byte ) = 0;

            using I2cEventCallback = delegate< void ( hal::Event event ) >;
            virtual void onEvent( I2cEventCallback callback ) = 0;

            virtual void initialize() = 0;
            virtual void enableInterrupt() = 0;
            virtual void disableInterrupt() = 0;
            virtual void master() = 0;
            virtual void slave() = 0;
            virtual void transferBlocking(uint8_t address, uint8_t *txBuffer, uint16_t txSize, uint8_t *rxBuffer, uint16_t rxSize) = 0;
        };
    }

    class I2c
    {
    public:
        I2c( hal::I2C* i2c );

        void beginTransaction( uint16_t address );
        void endTransaction();

        bool write( uint8_t byte );
        bool read( uint8_t *byte );

        uint32_t write( util::ByteBuffer& byteBuffer );
        uint32_t read( util::ByteBuffer& byteBuffer );

        void onI2cEvent( i2c::hal::Event event );

    private:
        enum class State
        {
            NONE,
            BEGIN,
            ADDRESSING,
            SENDING,
            RECEIVING,
            ERROR,
            ABORT,
            END,
        };

        enum class TransferType
        {
            NONE,
            TX,
            RX,
        };

        void beginWrite();
        void beginRead();
        void begin();

        util::ByteBuffer* buffer;
        hal::I2C* i2c;
        uint32_t currentDataIndex;
        uint16_t address;
        i2c::I2c::State state;
        i2c::I2c::TransferType transferType;

        bool transmitting;
    };
}

#endif // SOC_I2C_HPP_INCLUDED
