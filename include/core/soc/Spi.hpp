#pragma once
#ifndef SOC_SPI_HPP_INCLUDED
#define SOC_SPI_HPP_INCLUDED

#include <types>

namespace spi
{
    class Spi;

    namespace hal
    {
        void initialize();

        class SPI
        {
        public:
            virtual bool write( uint8_t byte ) = 0;
            virtual bool read( uint8_t *byte ) = 0;

            virtual uint32_t write( const uint8_t *buffer, uint32_t count ) = 0;
            virtual uint32_t read( uint8_t *buffer, uint32_t count ) = 0;

            virtual void beginTransaction() = 0;
            virtual void endTransaction() = 0;
        };
    }

    class Spi
    {
    public:
        Spi( hal::SPI* spi );

        // These are here to control when
        // SPI slave is selected and when deselected.
        void beginTransaction();
        void endTransaction();

        bool write( uint8_t byte );
        bool read( uint8_t *byte );

        void write( const uint8_t *buffer, uint32_t length );
        uint32_t read( uint8_t *buffer, uint32_t maxLength );

    private:
        hal::SPI* spi;
    };
}

#endif // SOC_SPI_HPP_INCLUDED
