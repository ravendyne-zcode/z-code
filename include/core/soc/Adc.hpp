#pragma once
#ifndef SOC_ADC_HPP_INCLUDED
#define SOC_ADC_HPP_INCLUDED

#include <types>
// #include <util/delegate.hpp>

namespace adc
{
    namespace hal
    {
        void initialize();
        extern const float Vref;
        extern const uint16_t maxValue;

        class Adc
        {
        public:
            virtual uint16_t read() = 0;
        };
    }

    class Adc
    {
    public:
        Adc( hal::Adc* adc );

        uint16_t readRaw();
        float value();

    private:
        hal::Adc* adc;
    };
}

#endif // SOC_ADC_HPP_INCLUDED
