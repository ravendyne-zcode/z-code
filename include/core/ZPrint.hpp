#pragma once
#ifndef Z_PRINT_HPP_INCLUDED
#define Z_PRINT_HPP_INCLUDED

/*! \file ZPrint.hpp
    \brief Print methods for easier writing of numbers and strings to streams.
*/

#include <cstdint>// uint*_t types
#include <cstddef> // size_t

#include <string>

//! Enumerates bases for conversion of integers to their string representation.
enum ConversionBase
{
    HEX = 16,
    DEC = 10,
    OCT =  8,
    BIN =  2,
};

//! Implements print methods to output stuff to streams.
/*! Usually subclassed by a stream-implementation class.
*/
class ZPrint
{
public:
    ZPrint();

    //! Writes one byte to the stream. Pure virtual, needs to be implemented in subclass.
    virtual size_t write(uint8_t c) = 0;
    //! Writes the length bytes from source to the stream.
    /*! Implemented in this class and uses virtual write(uint8_t) to do the work.

        Subclasses should override this with more efficient implementations,
        i.e. serial port i/o stream will have implementation that utilizes
        UART FIFOs.
    */
    virtual size_t write(uint8_t *source, size_t length);

    //! Writes '\0'-terminated string to the stream. Lower level method, used by print methods.
    size_t write(const char *source);
    //! **For Wiring/Arduino compatibility.** Use write(uint8_t *, size_t) instead of this.
    //! Writes '\0'-terminated string to the stream or up until length number of characters, whichever comes first.
    size_t write(const char *source, size_t length);


    //! Prints the char to a stream.
    size_t print(char c);
    //! Prints the '\0'-terminated char array to stream, excluding '\0' character at the end.
    size_t print(const char *text);
    //! Prints the string to stream.
    size_t print(const zcore::String &text);

    //! Prints 8-bit value to stream using DECimal base by default.
    size_t print(uint8_t value, ConversionBase base);
    
    //! Prints 8-bit value to stream using DECimal base by default.
    size_t print(int16_t value, ConversionBase base);
    //! Prints 8-bit value to stream using DECimal base by default.
    size_t print(uint16_t value, ConversionBase base);
    //! Prints 8-bit value to stream using DECimal base by default.
    size_t print(int32_t value, ConversionBase base);
    //! Prints 8-bit value to stream using DECimal base by default.
    size_t print(uint32_t value, ConversionBase base);
    
    //! Prints signed integer value to stream using DECimal base by default.
    size_t print(int value, ConversionBase base);
    //! Prints floating point value to stream in fixed decimal format with 2 decimal places as default.
    size_t print(double value, int decimalPlaces);


    //! Prints newline characterr: <LF> or 0x0A.
    size_t println();
    //! Same as print(char), adds newline at the end.
    size_t println(char c);
    //! Same as print(const char *), adds newline at the end.
    size_t println(const char *text);
    //! Same as print(const zcore::String &), adds newline at the end.
    size_t println(const zcore::String &text);
    //! Same as print(uint8_t, ConversionBase), adds newline at the end.
    size_t println(uint8_t value, ConversionBase base);
    //! Same as print(int16_t, ConversionBase), adds newline at the end.
    size_t println(int16_t value, ConversionBase base);
    //! Same as print(uint16_t, ConversionBase), adds newline at the end.
    size_t println(uint16_t value, ConversionBase base);
    //! Same as print(int32_t, ConversionBase), adds newline at the end.
    size_t println(int32_t value, ConversionBase base);
    //! Same as print(uint32_t, ConversionBase), adds newline at the end.
    size_t println(uint32_t value, ConversionBase base);
    //! Same as print(int, ConversionBase), adds newline at the end.
    size_t println(int value, ConversionBase base);
    //! Same as print(double, int), adds newline at the end.
    size_t println(double value, int decimalPlaces);

private:
    size_t convertWithBaseAndPrint(uint32_t value, ConversionBase base);

};

#endif // Z_PRINT_HPP_INCLUDED
