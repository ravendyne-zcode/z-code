#pragma once
#ifndef HELPER_MACROS_HPP_INCLUDED
#define HELPER_MACROS_HPP_INCLUDED

/*! \file helper_macros.hpp
    \brief (Mostly) GCC related helper macros for system level programming.
*/

/*
    More details:
    
    - https://gcc.gnu.org/onlinedocs/cpp/Stringification.html
    - https://gcc.gnu.org/onlinedocs/cpp/Concatenation.html
*/
#define __WEAK [[gnu::weak]]
#define __ALIAS(fn) [[gnu::weak]] [[gnu::alias(#fn)]]


#endif // HELPER_MACROS_HPP_INCLUDED
