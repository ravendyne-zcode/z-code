#pragma once
#ifndef SECTION_MACROS_HPP_INCLUDED
#define SECTION_MACROS_HPP_INCLUDED

/*! \file section_macros.hpp
    \brief Macros to help with placement of data/code to specific sections of Flash/RAM.
*/

// https://gcc.gnu.org/onlinedocs/cpp/Stringification.html
// https://gcc.gnu.org/onlinedocs/cpp/Concatenation.html

#define __SECTION_(sname) [[gnu::section("." #sname)]]
#define __SECTION_USED_(sname) [[gnu::used]] __SECTION_(sname)

#define __SECTION_GROUP_(sname, group) [[gnu::section("." #sname ".$" #group)]]
#define __SECTION_GROUP_TAG_(sname, group, tag) [[gnu::section("." #sname ".$" #group "." #tag)]]

/*
    Helpers to place ISR vectors array and
    some code between ISR vectors and CRP register in LPC MCUs.
*/
#define __SECTION_ISR_VECTOR_ __SECTION_USED_(isr_vector)
#define __SECTION_AFTER_VECTORS_ __SECTION_(after_vectors)

/*!
    Helper to place CRP register value at the correct location.
*/
#define __SECTION_CRP __SECTION_USED_(crp)

/*
    Helpers to place code into RAM.
    Group can be one of:
        - RAM2
        - USBRam
        - RAM3
        - PeripheralRam
*/
#define __SECTION_RAMFUNC_ __SECTION_(ramfunc)
#define __SECTION_RAMFUNC_GROUP_(group) __SECTION_(ramfunc, group)

/*
    Helpers for data, bss and noinit sections.
    Group can be one of:
        - RAM2
        - USBRam
        - RAM3
        - PeripheralRam
    Tag can be anything.
*/
#define __SECTION_DATA_(group) __SECTION_GROUP_(data, group)
#define __SECTION_DATA_TAG_(group, tag) __SECTION_GROUP_TAG_(data, group, tag)
#define __SECTION_BSS_(group) __SECTION_GROUP_(bss, group)
#define __SECTION_BSS_TAG_(group, tag) __SECTION_GROUP_TAG_(bss, group, tag)
#define __SECTION_NOINIT_(group) __SECTION_GROUP_(noinit, group)
#define __SECTION_NOINIT_TAG_(group, tag) __SECTION_GROUP_TAG_(noinit, group, tag)

/*
    Helpers for read-only and constant data.
    Placed after the application code (text sections) in FLASH.
    Group can be anything.
*/
#define __SECTION_RODATA_ __SECTION_(rodata)
#define __SECTION_RODATA_GROUP_(group) __SECTION_GROUP_(rodata, group)
#define __SECTION_CONSTDATA_ __SECTION_(constdata)
#define __SECTION_CONSTDATA_GROUP_(group) __SECTION_GROUP_(constdata, group)

#endif // SECTION_MACROS_HPP_INCLUDED
