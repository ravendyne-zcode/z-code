#pragma once
#ifndef LPC1347_PINS_HPP_INCLUDED
#define LPC1347_PINS_HPP_INCLUDED

#include <types>


// Initial pin modefunc values fof LPC1347.
// This should be defined by the board source file(s)
// since it depends on the board what initial functions
// are assigned to SOC pins.

typedef struct {
    // uint32_t port:8;        /* Pin port */
    // uint32_t pin:8;         /* Pin number */
    uint32_t modefunc/*:16*/;   /* Function and mode */
} SocPinMode;

#define PIN_FUNCTION_NONE     (0xFFFF)
#define PIN_FUNCTION_RESERVED (0x7FFF)
extern const SocPinMode InitialSocPinMode[];
extern const uint32_t InitialSocPinModeLength;

extern const uint16_t PinModeGPIO[];
extern const uint16_t PinModeADC[];
extern const uint16_t PinModePWM[];

#endif // LPC1347_PINS_HPP_INCLUDED
