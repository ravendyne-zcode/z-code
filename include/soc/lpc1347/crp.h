#ifndef CRP_H_
#define CRP_H_

/*! \file crp.h
    \brief Core Read Protection register value definition for LPC13xx MCUs.

    AN10968 - Using Code Read Protection in LPC1100 and LPC1300:
    http://www.nxp.com/documents/application_note/AN10968.pdf
*/

#define CRP_NO_CRP          0xFFFFFFFF
#define CRP_NO_ISP_MAGIC    0x4E697370
#define CRP_CRP1            0x12345678
#define CRP_CRP2            0x87654321

/************************************************************/
/**** DANGER CRP3 WILL LOCK PART TO ALL READS and WRITES ****/
#define CRP_CRP3_DANGER     0x43218765
/************************************************************/
 
#endif /* CRP_H_ */
