'use strict'

const fs = require('fs')
const shell = require( 'shelljs' )

if( fs.existsSync( 'package.json' ) )
{
    let package_cfg = JSON.parse( fs.readFileSync( 'package.json' ) )
    let git_sha = shell.exec('git rev-parse --short HEAD', {silent:true}).stdout.replace(/\n/g, '')
    let version = package_cfg.version + "_" + git_sha
    console.log( 'v'+version )

    let header = "var zbuild_version = '" + version + "';\n"
    let output = shell.cat( 'bin/zbuild.rollup.js' )

    fs.writeFileSync( 'bin/zbuild.js', header + output )
}
