#!/bin/bash

which arm-none-eabi-gcc > /dev/null 2>&1
WHAT=$?

if [ "$WHAT" == "1" ]; then
    if [ "$OSTYPE" == "darwin13" ]; then
        echo "OSX"
        export PATH=$PATH:/Users/user/Development/electron/gcc-arm-none-eabi-5_4-2016q3/bin
    elif [ "$OSTYPE" == "linux-gnu" ]; then
        echo "Linux"
        export PATH=$PATH:/usr/local/lpcxpresso_8.1.4_606/lpcxpresso/bin:/usr/local/lpcxpresso_8.1.4_606/lpcxpresso/tools/bin
        # export PATH=$PATH:/home/user/Development/electron/gcc-arm-none-eabi-5_4-2016q3/bin
    elif [ "$OS" == "Windows_NT" ]; then
        echo "Windows"
        export PATH=$PATH:/home/user/Development/electron/gcc-arm-none-eabi-5_4-2016q3/bin
    fi
fi


SCRIPTPATH=$( cd $(dirname $BASH_SOURCE) ; pwd -P )

export PATH=$PATH:$SCRIPTPATH

