#include <zprint>

#include <cstdint>
#include <climits>

// qemu print helpers
extern "C" int _write (int fd, char *ptr, int len);
int iowrite(const char *text) {return _write(0, (char*)text, 1000);}
int iowrite(char c) {return _write(0, &c, 1);}

extern "C" void CPU_RESET();

class MyPrint : public ZPrint
{
public:
    MyPrint(){};
    size_t write(uint8_t c)
    {
        iowrite(c);
        return 1;
    }    
};

MyPrint myPrint;

void setup()
{
    myPrint.println();
    myPrint.print('C');
    myPrint.println('c');

    myPrint.println();
    myPrint.print("ZPrint writes this... ");
    myPrint.println("continued on the same line.");
    myPrint.println("Text on new line.");

    myPrint.println();
    myPrint.println((uint32_t)0, DEC);
    myPrint.println((uint32_t)UINT32_MAX, DEC);
    myPrint.println((uint16_t)0, DEC);
    myPrint.println((uint16_t)UINT16_MAX, DEC);
    myPrint.println((uint8_t)0, DEC);
    myPrint.println((uint8_t)UINT8_MAX, DEC);

    myPrint.println();
    myPrint.println((int32_t)INT32_MIN, DEC);
    myPrint.println((int32_t)INT32_MAX, DEC);
    myPrint.println((int16_t)INT16_MIN, DEC);
    myPrint.println((int16_t)INT16_MAX, DEC);
    myPrint.println((int8_t)INT8_MIN, DEC);
    myPrint.println((int8_t)INT8_MAX, DEC);

    myPrint.println();
    myPrint.println((int32_t)INT32_MIN, HEX);
    myPrint.println((int32_t)INT32_MAX, HEX);
    myPrint.println((int16_t)INT16_MIN, HEX);
    myPrint.println((int16_t)INT16_MAX, HEX);
    myPrint.println((int8_t)INT8_MIN, HEX);
    myPrint.println((int8_t)INT8_MAX, HEX);

    myPrint.println();
    myPrint.println((int32_t)INT32_MIN, OCT);
    myPrint.println((int32_t)INT32_MAX, OCT);
    myPrint.println((int16_t)INT16_MIN, OCT);
    myPrint.println((int16_t)INT16_MAX, OCT);
    myPrint.println((int8_t)INT8_MIN, OCT);
    myPrint.println((int8_t)INT8_MAX, OCT);

    myPrint.println();
    myPrint.println((int)INT_MIN, DEC);
    myPrint.println((int)INT_MAX, DEC);

    myPrint.println();
    myPrint.println((uint8_t)128,BIN); // b10000000
    myPrint.println((uint8_t)384,BIN); // b10000000

    myPrint.println();
    myPrint.println(64738, HEX); // 0xFCE2
    myPrint.println(38, BIN); // b100110

    CPU_RESET();
}

void loop()
{
    return;
}