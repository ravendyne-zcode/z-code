#include <stdio.h>

#include <print>

volatile unsigned int * const UART0DR = (unsigned int *)0x4000c000;
 
void print_uart0(const char *s) {
 while(*s != '\0') { /* Loop until end of string */
 *UART0DR = (unsigned int)(*s); /* Transmit char */
 s++; /* Next char */
 }
}

static char hex [] = { '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9' ,'A', 'B', 'C', 'D', 'E', 'F' };
 
const char* toHex(int number)
{
    static char buffer[32];

    buffer[0] = 0;

    int len=0,k=0;
    do//for every 4 bits
    {
        //get the equivalent hex digit
        buffer[len] = hex[number & 0xF];
        len++;
        number>>=4;
    }while(number!=0);
    //since we get the digits in the wrong order reverse the digits in the buffer
    for(; k<len/2; k++)
    {//xor swapping
        buffer[k]^=buffer[len-k-1];
        buffer[len-k-1]^=buffer[k];
        buffer[k]^=buffer[len-k-1];
    }
    //null terminate the buffer and return the length in digits
    buffer[len]='\0';

    return buffer;
}


class Test
{
private:
    const char* location;
public:
    Test(const char* loc){ this->location = loc; };
    void sayHi(void){printf("Hi from %s class!!\n", this->location);}
};

Test testglobal("global");

class MyPrint : public ZPrint
{
public:
    MyPrint(){};
    size_t write(uint8_t c)
    {
        *UART0DR = c;
        return 0;
    }    
};

MyPrint myPrint;

void setup()
{
    int test_var = 135;
    test_var += 0x10; // =151

    print_uart0("0x");
    print_uart0(toHex(test_var)); // 0x97
    print_uart0("\n");

    // delay(1);
    print_uart0("Hello world from Cortex-M3!\n");

    printf("printf hola!\n");

    Test *testlocal = new Test("dynamic");
    testlocal->sayHi();
    testglobal.sayHi();

    myPrint.println("ZPrint writes this");
    myPrint.println("and this");

    myPrint.println((uint32_t)9999,DEC);
    myPrint.println(-9999,DEC);
    myPrint.println((uint32_t)64738,HEX); // 0xFCE2
    myPrint.println((uint32_t)10,BIN); // b1010
    myPrint.println((uint32_t)55,OCT); // 067
    myPrint.println((uint32_t)4116740058,BIN); // 11110101011000000111011111011010

    myPrint.println((uint8_t)128,BIN); // b10000000
    myPrint.println((uint8_t)384,BIN); // b10000000

}

// void loop()
// {

// }
